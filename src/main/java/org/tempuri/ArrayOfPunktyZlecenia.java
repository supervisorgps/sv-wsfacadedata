
package org.tempuri;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfPunktyZlecenia complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfPunktyZlecenia"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PunktyZlecenia" type="{http://tempuri.org/}PunktyZlecenia" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfPunktyZlecenia", propOrder = {
    "punktyZlecenia"
})
public class ArrayOfPunktyZlecenia implements Serializable{

    @XmlElement(name = "PunktyZlecenia")
    protected List<PunktyZlecenia> punktyZlecenia;

    /**
     * Gets the value of the punktyZlecenia property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the punktyZlecenia property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPunktyZlecenia().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PunktyZlecenia }
     * 
     * 
     */
    public List<PunktyZlecenia> getPunktyZlecenia() {
        if (punktyZlecenia == null) {
            punktyZlecenia = new ArrayList<PunktyZlecenia>();
        }
        return this.punktyZlecenia;
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ArrayOfPunktyZlecenia{");
        sb.append("punktyZlecenia=").append(Arrays.toString( punktyZlecenia.toArray()));
        sb.append('}');
        return sb.toString();
    }
}
