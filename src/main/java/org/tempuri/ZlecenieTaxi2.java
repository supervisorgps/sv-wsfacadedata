
package org.tempuri;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.Serializable;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ZlecenieTaxi2", propOrder = {
        "idZlecenia",
        "data",
        "dataOstatniejZmiany",
        "loginKierowcy",
        "opisTekstowy",
        "symbolZlecenia",
        "punktyZlecenia"
})
public class ZlecenieTaxi2 implements Serializable{


    public ZlecenieTaxi2(){}
    public ZlecenieTaxi2( ZlecenieTaxi stare){

        // nagłówek
        this.data = stare.getData();
        this.loginKierowcy = stare.getLoginKierowcy();
        this.opisTekstowy = stare.getOpisTekstowy();
        this.symbolZlecenia = stare.getSymbolZlecenia();
        this.idZlecenia = stare.getIdZlecenia();

        // pola
        if ( stare.getPunktyZlecenia() != null){

            this.punktyZlecenia = new ArrayOfPunktyZlecenia2( stare.getPunktyZlecenia());
        }

    }
    //
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar data;
    @XmlElement(name = "loginKierowcy", required = true, type = String.class, nillable = true)
    protected String loginKierowcy; //
    protected String opisTekstowy;
    protected String symbolZlecenia;
    @XmlElement(name = "idZlecenia", required = true, type = Integer.class, nillable = true)
    protected Integer idZlecenia;
    @XmlElement(name = "PunktyZlecenia")
    protected ArrayOfPunktyZlecenia2 punktyZlecenia;

    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    private XMLGregorianCalendar dataOstatniejZmiany;

    public XMLGregorianCalendar getDataOstatniejZmiany() {
        return dataOstatniejZmiany;
    }

    public void setDataOstatniejZmiany(XMLGregorianCalendar dataOstatniejZmiany) {
        this.dataOstatniejZmiany = dataOstatniejZmiany;
    }

    /**
     * Gets the value of the data property.
     *
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public XMLGregorianCalendar getData() {
        return data;
    }

    /**
     * Sets the value of the data property.
     *
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public void setData(XMLGregorianCalendar value) {
        this.data = value;
    }


    public String getLoginKierowcy() {
        return loginKierowcy;
    }

    public void setLoginKierowcy(String loginKierowcy) {
        this.loginKierowcy = loginKierowcy;
    }

    /**
     * Gets the value of the opisTekstowy property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getOpisTekstowy() {
        return opisTekstowy;
    }

    /**
     * Sets the value of the opisTekstowy property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setOpisTekstowy(String value) {
        this.opisTekstowy = value;
    }

    /**
     * Gets the value of the symbolZlecenia property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getSymbolZlecenia() {
        return symbolZlecenia;
    }

    /**
     * Sets the value of the symbolZlecenia property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setSymbolZlecenia(String value) {
        this.symbolZlecenia = value;
    }

    /**
     * Gets the value of the punktyZlecenia property.
     *
     * @return
     *     possible object is
     *     {@link ArrayOfPunktyZlecenia }
     *
     */
    public ArrayOfPunktyZlecenia2 getPunktyZlecenia() {
        return punktyZlecenia;
    }

    /**
     * Sets the value of the punktyZlecenia property.
     *
     * @param value
     *     allowed object is
     *     {@link ArrayOfPunktyZlecenia }
     *
     */
    public void setPunktyZlecenia(ArrayOfPunktyZlecenia2 value) {
        this.punktyZlecenia = value;
    }

    public Integer getIdZlecenia() {
        return idZlecenia;
    }

    public void setIdZlecenia(Integer idZlecenia) {
        this.idZlecenia = idZlecenia;
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ZlecenieTaxi2{");
        sb.append("data=").append(data);
        sb.append(", loginKierowcy='").append(loginKierowcy).append('\'');
        sb.append(", opisTekstowy='").append(opisTekstowy).append('\'');
        sb.append(", symbolZlecenia='").append(symbolZlecenia).append('\'');
        sb.append(", idZlecenia=").append(idZlecenia);
        sb.append(", punktyZlecenia=").append(punktyZlecenia);
        sb.append(", dataOstatniejZmiany=").append(dataOstatniejZmiany);
        sb.append('}');
        return sb.toString();
    }
}