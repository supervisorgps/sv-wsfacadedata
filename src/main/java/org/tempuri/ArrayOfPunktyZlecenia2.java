
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * <p>Java class for ArrayOfPunktyZlecenia complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfPunktyZlecenia"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PunktyZlecenia" type="{http://tempuri.org/}PunktyZlecenia" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfPunktyZlecenia2", propOrder = {
    "punktyZlecenia"
})
public class ArrayOfPunktyZlecenia2 implements Serializable{

    public ArrayOfPunktyZlecenia2(){}

    public ArrayOfPunktyZlecenia2(ArrayOfPunktyZlecenia starePunkty){

        if ( starePunkty != null && starePunkty.getPunktyZlecenia() != null && !starePunkty.getPunktyZlecenia().isEmpty()){

            this.punktyZlecenia = new ArrayList<PunktZlecenia2>( starePunkty.getPunktyZlecenia().size());

            for(PunktyZlecenia staryPunkt : starePunkty.getPunktyZlecenia()){

                this.punktyZlecenia.add( new PunktZlecenia2( staryPunkt));
            }
        }
    }
    @XmlElement(name = "PunktyZlecenia2")
    protected List<PunktZlecenia2> punktyZlecenia;

    /**
     * Gets the value of the punktyZlecenia property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the punktyZlecenia property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPunktyZlecenia().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PunktyZlecenia }
     * 
     * 
     */
    public List<PunktZlecenia2> getPunktyZlecenia() {
        if (punktyZlecenia == null) {
            punktyZlecenia = new ArrayList<PunktZlecenia2>();
        }
        return this.punktyZlecenia;
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ArrayOfPunktyZlecenia2{");
        sb.append("punktyZlecenia=").append(Arrays.toString( punktyZlecenia.toArray()));
        sb.append('}');
        return sb.toString();
    }
}
