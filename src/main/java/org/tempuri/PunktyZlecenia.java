
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.Serializable;


/**
 * <p>Java class for PunktyZlecenia complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PunktyZlecenia"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="symbolPunktu" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="adresPunktuOpis" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="dataCZasWizytyOd" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="dataCZasWizytyDo" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="idpunktu" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="Pasazerowe" type="{http://tempuri.org/}ArrayOfPasazerowie" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PunktyZlecenia", propOrder = {
        "symbolPunktu",
        "adresPunktuOpis",
        "longitude",
        "latiitude",
        "dataCZasWizytyOd",
        "dataCZasWizytyDo",
        "uwagiDoPunktu",
        "indeksPunktu",
        "pasazerowe"
})
public class PunktyZlecenia implements Serializable{

    protected String symbolPunktu;
    protected String adresPunktuOpis;

    protected String uwagiDoPunktu;

    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataCZasWizytyOd;

    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataCZasWizytyDo;

    //    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected String longitude;
    protected String latiitude;

    @XmlElement(name = "Pasazerowe")
    protected ArrayOfPasazerowie pasazerowe;

    @XmlElement(name = "indeksPunktu", required = true, type = Integer.class, nillable = true)
    private Integer indeksPunktu;

    /**
     * Gets the value of the symbolPunktu property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getSymbolPunktu() {
        return symbolPunktu;
    }

    /**
     * Sets the value of the symbolPunktu property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setSymbolPunktu(String value) {
        this.symbolPunktu = value;
    }

    /**
     * Gets the value of the adresPunktuOpis property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getAdresPunktuOpis() {
        return adresPunktuOpis;
    }

    /**
     * Sets the value of the adresPunktuOpis property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setAdresPunktuOpis(String value) {
        this.adresPunktuOpis = value;
    }

    /**
     * Gets the value of the dataCZasWizytyOd property.
     *
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public XMLGregorianCalendar getDataCZasWizytyOd() {
        return dataCZasWizytyOd;
    }

    /**
     * Sets the value of the dataCZasWizytyOd property.
     *
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public void setDataCZasWizytyOd(XMLGregorianCalendar value) {
        this.dataCZasWizytyOd = value;
    }

    /**
     * Gets the value of the dataCZasWizytyDo property.
     *
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public XMLGregorianCalendar getDataCZasWizytyDo() {
        return dataCZasWizytyDo;
    }

    /**
     * Sets the value of the dataCZasWizytyDo property.
     *
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public void setDataCZasWizytyDo(XMLGregorianCalendar value) {
        this.dataCZasWizytyDo = value;
    }

    /**
     * Gets the value of the pasazerowe property.
     *
     * @return
     *     possible object is
     *     {@link ArrayOfPasazerowie }
     *
     */
    public ArrayOfPasazerowie getPasazerowe() {
        return pasazerowe;
    }

    /**
     * Sets the value of the pasazerowe property.
     *
     * @param value
     *     allowed object is
     *     {@link ArrayOfPasazerowie }
     *
     */
    public void setPasazerowe(ArrayOfPasazerowie value) {
        this.pasazerowe = value;
    }

    public String getUwagiDoPunktu() {
        return uwagiDoPunktu;
    }

    public void setUwagiDoPunktu(String uwagiDoPunktu) {
        this.uwagiDoPunktu = uwagiDoPunktu;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatiitude() {
        return latiitude;
    }

    public void setLatiitude(String latiitude) {
        this.latiitude = latiitude;
    }

    public Integer getIndeksPunktu() {
        return indeksPunktu;
    }

    public void setIndeksPunktu(Integer indeksPunktu) {
        this.indeksPunktu = indeksPunktu;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PunktyZlecenia{");
        sb.append("symbolPunktu='").append(symbolPunktu).append('\'');
        sb.append(", adresPunktuOpis='").append(adresPunktuOpis).append('\'');
        sb.append(", uwagiDoPunktu='").append(uwagiDoPunktu).append('\'');
        sb.append(", dataCZasWizytyOd=").append(dataCZasWizytyOd);
        sb.append(", dataCZasWizytyDo=").append(dataCZasWizytyDo);
        sb.append(", longitude='").append(longitude).append('\'');
        sb.append(", latiitude='").append(latiitude).append('\'');
        sb.append(", indeksPunktu ='").append(indeksPunktu).append('\'');
        sb.append(", pasazerowe=").append(pasazerowe != null ? pasazerowe.toString() : "Brak pasażerów");
        sb.append('}');
        return sb.toString();
    }
}
