
package org.tempuri;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.Serializable;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PunktZlecenia2", propOrder = {
        "symbolPunktu",
        "adresPunktuOpis",
        "longitude",
        "latiitude",
        "dataCZasWizytyOd",
        "dataCZasWizytyDo",
        "rozliczeniowy",
        "dystansNarastajaco",
        "czasNarastajaco",
        "zgodaNaAutostrade",
        "uwagiDoPunktu",
        "indeksPunktu",
        "pasazerowe"
})
public class PunktZlecenia2 implements Serializable{

    public PunktZlecenia2() {
    }

    public PunktZlecenia2(PunktyZlecenia staryPunkt) {
        this.symbolPunktu = staryPunkt.getSymbolPunktu();
        this.adresPunktuOpis = staryPunkt.getAdresPunktuOpis();
        this.uwagiDoPunktu = staryPunkt.getUwagiDoPunktu();
        this.dataCZasWizytyOd = staryPunkt.getDataCZasWizytyOd();
        this.dataCZasWizytyDo = staryPunkt.getDataCZasWizytyDo();
        this.longitude = staryPunkt.getLongitude();
        this.latiitude = staryPunkt.getLatiitude();
        this.indeksPunktu = staryPunkt.getIndeksPunktu();
        this.pasazerowe = staryPunkt.getPasazerowe();
    }

    protected String symbolPunktu;
    protected String adresPunktuOpis;

    protected String uwagiDoPunktu;

    @XmlElement(name = "rozliczeniowy", required = true, type = Boolean.class, nillable = true)
    protected Boolean rozliczeniowy;

    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataCZasWizytyOd;

    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataCZasWizytyDo;

    //    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected String longitude;
    protected String latiitude;

    @XmlElement(name = "Pasazerowe")
    protected ArrayOfPasazerowie pasazerowe;

    @XmlElement(name = "indeksPunktu", required = true, type = Integer.class, nillable = true)
    private Integer indeksPunktu;

    @XmlElement(name = "zgodaNaAutostrade", required = true, type = Boolean.class, nillable = true)
    private Boolean zgodaNaAutostrade;

    @XmlElement(name = "dystansNarastajaco", required = true, type = Double.class, nillable = true)
    private Double dystansNarastajaco;

    @XmlElement(name = "czasNarastajaco", required = true, type = Double.class, nillable = true)
    private Double czasNarastajaco;

    public Double getCzasNarastajaco() {
        return czasNarastajaco;
    }

    public void setCzasNarastajaco(Double czasNarastajaco) {
        this.czasNarastajaco = czasNarastajaco;
    }

    public Double getDystansNarastajaco() {
        return dystansNarastajaco;
    }

    public void setDystansNarastajaco(Double dystansNarastajaco) {
        this.dystansNarastajaco = dystansNarastajaco;
    }

    public Boolean getZgodaNaAutostrade() {
        return zgodaNaAutostrade;
    }

    public void setZgodaNaAutostrade(Boolean zgodaNaAutostrade) {
        this.zgodaNaAutostrade = zgodaNaAutostrade;
    }

    /**
     * Gets the value of the symbolPunktu property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getSymbolPunktu() {
        return symbolPunktu;
    }

    /**
     * Sets the value of the symbolPunktu property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setSymbolPunktu(String value) {
        this.symbolPunktu = value;
    }

    /**
     * Gets the value of the adresPunktuOpis property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getAdresPunktuOpis() {
        return adresPunktuOpis;
    }

    /**
     * Sets the value of the adresPunktuOpis property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setAdresPunktuOpis(String value) {
        this.adresPunktuOpis = value;
    }

    /**
     * Gets the value of the dataCZasWizytyOd property.
     *
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public XMLGregorianCalendar getDataCZasWizytyOd() {
        return dataCZasWizytyOd;
    }

    /**
     * Sets the value of the dataCZasWizytyOd property.
     *
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public void setDataCZasWizytyOd(XMLGregorianCalendar value) {
        this.dataCZasWizytyOd = value;
    }

    /**
     * Gets the value of the dataCZasWizytyDo property.
     *
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public XMLGregorianCalendar getDataCZasWizytyDo() {
        return dataCZasWizytyDo;
    }

    /**
     * Sets the value of the dataCZasWizytyDo property.
     *
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public void setDataCZasWizytyDo(XMLGregorianCalendar value) {
        this.dataCZasWizytyDo = value;
    }

    /**
     * Gets the value of the pasazerowe property.
     *
     * @return
     *     possible object is
     *     {@link ArrayOfPasazerowie }
     *
     */
    public ArrayOfPasazerowie getPasazerowe() {
        return pasazerowe;
    }

    /**
     * Sets the value of the pasazerowe property.
     *
     * @param value
     *     allowed object is
     *     {@link ArrayOfPasazerowie }
     *
     */
    public void setPasazerowe(ArrayOfPasazerowie value) {
        this.pasazerowe = value;
    }

    public String getUwagiDoPunktu() {
        return uwagiDoPunktu;
    }

    public void setUwagiDoPunktu(String uwagiDoPunktu) {
        this.uwagiDoPunktu = uwagiDoPunktu;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatiitude() {
        return latiitude;
    }

    public void setLatiitude(String latiitude) {
        this.latiitude = latiitude;
    }

    public Integer getIndeksPunktu() {
        return indeksPunktu;
    }

    public void setIndeksPunktu(Integer indeksPunktu) {
        this.indeksPunktu = indeksPunktu;
    }

    public Boolean getRozliczeniowy() {
        return rozliczeniowy;
    }

    public void setRozliczeniowy(Boolean rozliczeniowy) {
        this.rozliczeniowy = rozliczeniowy;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PunktZlecenia2{");
        sb.append("symbolPunktu='").append(symbolPunktu).append('\'');
        sb.append(", adresPunktuOpis='").append(adresPunktuOpis).append('\'');
        sb.append(", uwagiDoPunktu='").append(uwagiDoPunktu).append('\'');
        sb.append(", rozliczeniowy=").append(rozliczeniowy);
        sb.append(", dataCZasWizytyOd=").append(dataCZasWizytyOd);
        sb.append(", dataCZasWizytyDo=").append(dataCZasWizytyDo);
        sb.append(", longitude='").append(longitude).append('\'');
        sb.append(", latiitude='").append(latiitude).append('\'');
        sb.append(", pasazerowe=").append(pasazerowe);
        sb.append(", indeksPunktu=").append(indeksPunktu);
        sb.append(", zgodaNaAutostrade=").append(zgodaNaAutostrade);
        sb.append(", dystansNarastajaco=").append(dystansNarastajaco);
        sb.append(", czasNarastajaco=").append(czasNarastajaco);
        sb.append('}');
        return sb.toString();
    }


}
