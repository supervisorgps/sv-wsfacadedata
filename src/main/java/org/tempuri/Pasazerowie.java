
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;


/**
 * <p>Java class for Pasazerowie complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Pasazerowie"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="idPasazera" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="iloscKmPrzedPowiadomieniem" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="nazwaPasazera1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="nazwaPasazera2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="numerTelefonuDoPowiadomienia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="powiadomicPrzedPrzyjazdem" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="dodatkoweInformacje1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="dodatkoweInformacje" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Pasazerowie", propOrder = {
    "idPasazera",
    "iloscKmPrzedPowiadomieniem",
    "nazwaPasazera1",
    "nazwaPasazera2",
    "numerTelefonuDoPowiadomienia",
    "powiadomicPrzedPrzyjazdem",
    "dodatkoweInformacje1",
    "dodatkoweInformacje",
    "ruchPasazera"
})
public class Pasazerowie implements Serializable{

    protected String idPasazera;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer iloscKmPrzedPowiadomieniem;
    protected String nazwaPasazera1;
    protected String nazwaPasazera2;
    protected String numerTelefonuDoPowiadomienia;
    protected Boolean powiadomicPrzedPrzyjazdem;
    protected String dodatkoweInformacje1;
    protected String dodatkoweInformacje;
    private String ruchPasazera;

    /**
     * Gets the value of the idPasazera property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdPasazera() {
        return idPasazera;
    }

    /**
     * Sets the value of the idPasazera property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdPasazera(String value) {
        this.idPasazera = value;
    }

    /**
     * Gets the value of the iloscKmPrzedPowiadomieniem property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIloscKmPrzedPowiadomieniem() {
        return iloscKmPrzedPowiadomieniem;
    }

    /**
     * Sets the value of the iloscKmPrzedPowiadomieniem property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIloscKmPrzedPowiadomieniem(Integer value) {
        this.iloscKmPrzedPowiadomieniem = value;
    }

    /**
     * Gets the value of the nazwaPasazera1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNazwaPasazera1() {
        return nazwaPasazera1;
    }

    /**
     * Sets the value of the nazwaPasazera1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNazwaPasazera1(String value) {
        this.nazwaPasazera1 = value;
    }

    /**
     * Gets the value of the nazwaPasazera2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNazwaPasazera2() {
        return nazwaPasazera2;
    }

    /**
     * Sets the value of the nazwaPasazera2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNazwaPasazera2(String value) {
        this.nazwaPasazera2 = value;
    }

    /**
     * Gets the value of the numerTelefonuDoPowiadomienia property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumerTelefonuDoPowiadomienia() {
        return numerTelefonuDoPowiadomienia;
    }

    /**
     * Sets the value of the numerTelefonuDoPowiadomienia property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumerTelefonuDoPowiadomienia(String value) {
        this.numerTelefonuDoPowiadomienia = value;
    }

    /**
     * Gets the value of the powiadomicPrzedPrzyjazdem property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Boolean getPowiadomicPrzedPrzyjazdem() {
        return powiadomicPrzedPrzyjazdem;
    }

    /**
     * Sets the value of the powiadomicPrzedPrzyjazdem property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPowiadomicPrzedPrzyjazdem(Boolean value) {
        this.powiadomicPrzedPrzyjazdem = value;
    }

    /**
     * Gets the value of the dodatkoweInformacje1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDodatkoweInformacje1() {
        return dodatkoweInformacje1;
    }

    /**
     * Sets the value of the dodatkoweInformacje1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDodatkoweInformacje1(String value) {
        this.dodatkoweInformacje1 = value;
    }

    /**
     * Gets the value of the dodatkoweInformacje property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDodatkoweInformacje() {
        return dodatkoweInformacje;
    }

    /**
     * Sets the value of the dodatkoweInformacje property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDodatkoweInformacje(String value) {
        this.dodatkoweInformacje = value;
    }

    public String getRuchPasazera() {
        return ruchPasazera;
    }

    public void setRuchPasazera(String ruchPasazera) {
        this.ruchPasazera = ruchPasazera;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Pasazerowie{");
        sb.append("idPasazera='").append(idPasazera).append('\'');
        sb.append(", iloscKmPrzedPowiadomieniem=").append(iloscKmPrzedPowiadomieniem);
        sb.append(", nazwaPasazera1='").append(nazwaPasazera1).append('\'');
        sb.append(", nazwaPasazera2='").append(nazwaPasazera2).append('\'');
        sb.append(", numerTelefonuDoPowiadomienia='").append(numerTelefonuDoPowiadomienia).append('\'');
        sb.append(", powiadomicPrzedPrzyjazdem=").append(powiadomicPrzedPrzyjazdem);
        sb.append(", dodatkoweInformacje1='").append(dodatkoweInformacje1).append('\'');
        sb.append(", dodatkoweInformacje='").append(dodatkoweInformacje).append('\'');
        sb.append(", ruchPasazera='").append(ruchPasazera).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
