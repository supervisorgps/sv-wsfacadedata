
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.Serializable;


/**
 * <p>Java class for ZlecenieTaxi complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ZlecenieTaxi"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="data" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="id_kierowcy" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="opisTekstowy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="symbolZlecenia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PunktyZlecenia" type="{http://tempuri.org/}ArrayOfPunktyZlecenia" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ZlecenieTaxi", propOrder = {
        "idZlecenia",
        "data",
        "loginKierowcy",
        "opisTekstowy",
        "symbolZlecenia",
        "punktyZlecenia"
})
public class ZlecenieTaxi implements Serializable{

    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar data;
    @XmlElement(name = "loginKierowcy", required = true, type = String.class, nillable = true)
    protected String loginKierowcy; //
    protected String opisTekstowy;
    protected String symbolZlecenia;
    @XmlElement(name = "idZlecenia", required = true, type = Integer.class, nillable = true)
    protected Integer idZlecenia;
    @XmlElement(name = "PunktyZlecenia")
    protected ArrayOfPunktyZlecenia punktyZlecenia;

    /**
     * Gets the value of the data property.
     *
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public XMLGregorianCalendar getData() {
        return data;
    }

    /**
     * Sets the value of the data property.
     *
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public void setData(XMLGregorianCalendar value) {
        this.data = value;
    }


    public String getLoginKierowcy() {
        return loginKierowcy;
    }

    public void setLoginKierowcy(String loginKierowcy) {
        this.loginKierowcy = loginKierowcy;
    }

    /**
     * Gets the value of the opisTekstowy property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getOpisTekstowy() {
        return opisTekstowy;
    }

    /**
     * Sets the value of the opisTekstowy property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setOpisTekstowy(String value) {
        this.opisTekstowy = value;
    }

    /**
     * Gets the value of the symbolZlecenia property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getSymbolZlecenia() {
        return symbolZlecenia;
    }

    /**
     * Sets the value of the symbolZlecenia property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setSymbolZlecenia(String value) {
        this.symbolZlecenia = value;
    }

    /**
     * Gets the value of the punktyZlecenia property.
     *
     * @return
     *     possible object is
     *     {@link ArrayOfPunktyZlecenia }
     *
     */
    public ArrayOfPunktyZlecenia getPunktyZlecenia() {
        return punktyZlecenia;
    }

    /**
     * Sets the value of the punktyZlecenia property.
     *
     * @param value
     *     allowed object is
     *     {@link ArrayOfPunktyZlecenia }
     *
     */
    public void setPunktyZlecenia(ArrayOfPunktyZlecenia value) {
        this.punktyZlecenia = value;
    }

    public Integer getIdZlecenia() {
        return idZlecenia;
    }

    public void setIdZlecenia(Integer idZlecenia) {
        this.idZlecenia = idZlecenia;
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ZlecenieTaxi{");
        sb.append("data=").append(data);
        sb.append(", idKierowcy=").append( loginKierowcy);
        sb.append(", opisTekstowy='").append(opisTekstowy).append('\'');
        sb.append(", symbolZlecenia='").append(symbolZlecenia).append('\'');
        sb.append(", idZlecenia=").append(idZlecenia);
        sb.append(", punktyZlecenia=").append(punktyZlecenia.toString());
        sb.append('}');
        return sb.toString();
    }
}
