
package org.tempuri;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfPasazerowie complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfPasazerowie"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Pasazerowie" type="{http://tempuri.org/}Pasazerowie" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfPasazerowie", propOrder = {
    "pasazerowie"
})
public class ArrayOfPasazerowie implements Serializable{

    @XmlElement(name = "Pasazerowie")
    protected List<Pasazerowie> pasazerowie;

    /**
     * Gets the value of the pasazerowie property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the pasazerowie property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPasazerowie().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Pasazerowie }
     * 
     * 
     */
    public List<Pasazerowie> getPasazerowie() {
        if (pasazerowie == null) {
            pasazerowie = new ArrayList<Pasazerowie>();
        }
        return this.pasazerowie;
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ArrayOfPasazerowie{");
        sb.append("pasazerowie=").append(Arrays.toString(pasazerowie.toArray()));
        sb.append('}');
        return sb.toString();
    }
}
