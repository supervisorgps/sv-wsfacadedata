package pl.supervisor.frontend.gwt.model.client.basemodels;

import java.io.Serializable;
import java.util.Date;

/**
 * Zawartość zgodna z encją PowiadomienieSMS
 */
public class SmsNotificationTO implements Serializable{

    private String nrTelefonu;
    private String nazwaOdbiorcy;
    private Date czasZlecenia;
    private Date czasRealizacji;
    private Integer kodStatusuRealizacji;
    private String opisStatusuRealizacji;
    private String tresc;
    private Integer kategoria;
    private Integer klnId;
    private String dodatkoweDane1;
    private String dodatkoweDane2;
    private String idZewnetrzne;

    public String getNrTelefonu() {
        return nrTelefonu;
    }

    public void setNrTelefonu(String nrTelefonu) {
        this.nrTelefonu = nrTelefonu;
    }

    public String getNazwaOdbiorcy() {
        return nazwaOdbiorcy;
    }

    public void setNazwaOdbiorcy(String nazwaOdbiorcy) {
        this.nazwaOdbiorcy = nazwaOdbiorcy;
    }

    public Date getCzasZlecenia() {
        return czasZlecenia;
    }

    public void setCzasZlecenia(Date czasZlecenia) {
        this.czasZlecenia = czasZlecenia;
    }

    public Date getCzasRealizacji() {
        return czasRealizacji;
    }

    public void setCzasRealizacji(Date czasRealizacji) {
        this.czasRealizacji = czasRealizacji;
    }

    public Integer getKodStatusuRealizacji() {
        return kodStatusuRealizacji;
    }

    public void setKodStatusuRealizacji(Integer kodStatusuRealizacji) {
        this.kodStatusuRealizacji = kodStatusuRealizacji;
    }

    public String getOpisStatusuRealizacji() {
        return opisStatusuRealizacji;
    }

    public void setOpisStatusuRealizacji(String opisStatusuRealizacji) {
        this.opisStatusuRealizacji = opisStatusuRealizacji;
    }

    public String getTresc() {
        return tresc;
    }

    public void setTresc(String tresc) {
        this.tresc = tresc;
    }

    public Integer getKategoria() {
        return kategoria;
    }

    public void setKategoria(Integer kategoria) {
        this.kategoria = kategoria;
    }

    public Integer getKlnId() {
        return klnId;
    }

    public void setKlnId(Integer klnId) {
        this.klnId = klnId;
    }

    public String getDodatkoweDane1() {
        return dodatkoweDane1;
    }

    public void setDodatkoweDane1(String dodatkoweDane1) {
        this.dodatkoweDane1 = dodatkoweDane1;
    }

    public String getDodatkoweDane2() {
        return dodatkoweDane2;
    }

    public void setDodatkoweDane2(String dodatkoweDane2) {
        this.dodatkoweDane2 = dodatkoweDane2;
    }

    public String getIdZewnetrzne() {
        return idZewnetrzne;
    }

    public void setIdZewnetrzne(String idZewnetrzne) {
        this.idZewnetrzne = idZewnetrzne;
    }
}
