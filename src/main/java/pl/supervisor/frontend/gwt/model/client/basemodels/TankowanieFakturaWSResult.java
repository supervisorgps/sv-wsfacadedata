package pl.supervisor.frontend.gwt.model.client.basemodels;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by norbertl on 26.10.16.
 */
public class TankowanieFakturaWSResult implements Serializable{

    private Boolean tankowanie;
    private Boolean wyliczone;
    private Date dataFaktury;
    private String nrFaktury;
    private Double kwotaZaplacona;
    private Double cenaZaLitr;
    private Double iloscLitrowFaktura;
    private Double iloscLitrowZaakceptowana;
    private Date dataCzasAkceptacji;
    private Date dataWprowadzenia;
    private Boolean zaakceptowana;
    private String zdarzenie;
    private Double wielkoscBaku;
    private Integer przebiegPojazduFaktura;
    private Boolean zmieniono;
    private Boolean poZdarzeniu;
    private Boolean pozaZakresem;
    private String zakresDatTankowania;
    private Date poczatekIncydentu;
    private Date koniecIncydentu;

    public Boolean getTankowanie() {
        return tankowanie;
    }

    public void setTankowanie(Boolean tankowanie) {
        this.tankowanie = tankowanie;
    }

    public Boolean getWyliczone() {
        return wyliczone;
    }

    public void setWyliczone(Boolean wyliczone) {
        this.wyliczone = wyliczone;
    }

    public Date getDataFaktury() {
        return dataFaktury;
    }

    public void setDataFaktury(Date dataFaktury) {
        this.dataFaktury = dataFaktury;
    }

    public String getNrFaktury() {
        return nrFaktury;
    }

    public void setNrFaktury(String nrFaktury) {
        this.nrFaktury = nrFaktury;
    }

    public Double getKwotaZaplacona() {
        return kwotaZaplacona;
    }

    public void setKwotaZaplacona(Double kwotaZaplacona) {
        this.kwotaZaplacona = kwotaZaplacona;
    }

    public Double getCenaZaLitr() {
        return cenaZaLitr;
    }

    public void setCenaZaLitr(Double cenaZaLitr) {
        this.cenaZaLitr = cenaZaLitr;
    }

    public Double getIloscLitrowFaktura() {
        return iloscLitrowFaktura;
    }

    public void setIloscLitrowFaktura(Double iloscLitrowFaktura) {
        this.iloscLitrowFaktura = iloscLitrowFaktura;
    }

    public Double getIloscLitrowZaakceptowana() {
        return iloscLitrowZaakceptowana;
    }

    public void setIloscLitrowZaakceptowana(Double iloscLitrowZaakceptowana) {
        this.iloscLitrowZaakceptowana = iloscLitrowZaakceptowana;
    }

    public Date getDataCzasAkceptacji() {
        return dataCzasAkceptacji;
    }

    public void setDataCzasAkceptacji(Date dataCzasAkceptacji) {
        this.dataCzasAkceptacji = dataCzasAkceptacji;
    }

    public Date getDataWprowadzenia() {
        return dataWprowadzenia;
    }

    public void setDataWprowadzenia(Date dataWprowadzenia) {
        this.dataWprowadzenia = dataWprowadzenia;
    }

    public Boolean getZaakceptowana() {
        return zaakceptowana;
    }

    public void setZaakceptowana(Boolean zaakceptowana) {
        this.zaakceptowana = zaakceptowana;
    }

    public String getZdarzenie() {
        return zdarzenie;
    }

    public void setZdarzenie(String zdarzenie) {
        this.zdarzenie = zdarzenie;
    }

    public Double getWielkoscBaku() {
        return wielkoscBaku;
    }

    public void setWielkoscBaku(Double wielkoscBaku) {
        this.wielkoscBaku = wielkoscBaku;
    }

    public Integer getPrzebiegPojazduFaktura() {
        return przebiegPojazduFaktura;
    }

    public void setPrzebiegPojazduFaktura(Integer przebiegPojazduFaktura) {
        this.przebiegPojazduFaktura = przebiegPojazduFaktura;
    }

    public Boolean getZmieniono() {
        return zmieniono;
    }

    public void setZmieniono(Boolean zmieniono) {
        this.zmieniono = zmieniono;
    }

    public Boolean getPoZdarzeniu() {
        return poZdarzeniu;
    }

    public void setPoZdarzeniu(Boolean poZdarzeniu) {
        this.poZdarzeniu = poZdarzeniu;
    }

    public Boolean getPozaZakresem() {
        return pozaZakresem;
    }

    public void setPozaZakresem(Boolean pozaZakresem) {
        this.pozaZakresem = pozaZakresem;
    }

    public String getZakresDatTankowania() {
        return zakresDatTankowania;
    }

    public void setZakresDatTankowania(String zakresDatTankowania) {
        this.zakresDatTankowania = zakresDatTankowania;
    }

    public Date getPoczatekIncydentu() {
        return poczatekIncydentu;
    }

    public void setPoczatekIncydentu(Date poczatekIncydentu) {
        this.poczatekIncydentu = poczatekIncydentu;
    }

    public Date getKoniecIncydentu() {
        return koniecIncydentu;
    }

    public void setKoniecIncydentu(Date koniecIncydentu) {
        this.koniecIncydentu = koniecIncydentu;
    }
}
