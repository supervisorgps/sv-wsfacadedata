package pl.supervisor.frontend.gwt.model.client.basemodels;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import sv.helper.JsonDateSerializer;

import java.util.Date;

public class WizytaDaneWSDTO implements java.io.Serializable{

	private Long idWizyty;
	private String symbolTrasy; // z TWT
	private String punktWlasnySymbol;
	private String punktWlasnyNazwa;
	private Date minCzasRozpoczeciaWgTrasy;
	private Date maxCzasRozpoczeciaWgTrasy;
	private Date minCzasRozpoczeciaWgWizyty;
	private Date maxCzasRozpoczeciaWgWizyty;
	private String numerRejestracyjnyAuta;
	private Boolean czasKrytyczny;
	private Integer dystans = 0;
	private Integer indeksWizyty;
	
	public WizytaDaneWSDTO(){
	}

	public WizytaDaneWSDTO(Long idWizyty, String symbolTrasy,
						   String punktWlasnySymbol, String punktWlasnyNazwa,
						   Date minCzasRozpoczeciaWgTrasy, Date maxCzasRozpoczeciaWgTrasy,
						   Date minCzasRozpoczeciaWgWizyty, Date maxCzasRozpoczeciaWgWizyty,
						   String numerRejestracyjnyAuta, Boolean czasKrytyczny,
						   Integer dystans, Integer indeksWizyty) {
		super();
		this.idWizyty = idWizyty;
		this.symbolTrasy = symbolTrasy;
		this.punktWlasnySymbol = punktWlasnySymbol;
		this.punktWlasnyNazwa = punktWlasnyNazwa;
		this.minCzasRozpoczeciaWgTrasy = minCzasRozpoczeciaWgTrasy;
		this.maxCzasRozpoczeciaWgTrasy = maxCzasRozpoczeciaWgTrasy;
		this.minCzasRozpoczeciaWgWizyty = minCzasRozpoczeciaWgWizyty;
		this.maxCzasRozpoczeciaWgWizyty = maxCzasRozpoczeciaWgWizyty;
		this.numerRejestracyjnyAuta = numerRejestracyjnyAuta;
		this.czasKrytyczny = czasKrytyczny;
		this.dystans = dystans;
		this.indeksWizyty = indeksWizyty;
	}

	public WizytaDaneWSDTO(Long idWizyty, String symbolTrasy,
						   String punktWlasnySymbol, String punktWlasnyNazwa,
						   Date minCzasRozpoczeciaWgTrasy, Date maxCzasRozpoczeciaWgTrasy,
						   Date minCzasRozpoczeciaWgWizyty, Date maxCzasRozpoczeciaWgWizyty,
						   Boolean czasKrytyczny,
						   Integer dystans, Integer indeksWizyty) {
		super();
		this.idWizyty = idWizyty;
		this.symbolTrasy = symbolTrasy;
		this.punktWlasnySymbol = punktWlasnySymbol;
		this.punktWlasnyNazwa = punktWlasnyNazwa;
		this.minCzasRozpoczeciaWgTrasy = minCzasRozpoczeciaWgTrasy;
		this.maxCzasRozpoczeciaWgTrasy = maxCzasRozpoczeciaWgTrasy;
		this.minCzasRozpoczeciaWgWizyty = minCzasRozpoczeciaWgWizyty;
		this.maxCzasRozpoczeciaWgWizyty = maxCzasRozpoczeciaWgWizyty;
		this.czasKrytyczny = czasKrytyczny;
		this.dystans = dystans;
		this.indeksWizyty = indeksWizyty;
	}

	public Long getIdWizyty() {
		return idWizyty;
	}

	public void setIdWizyty(Long idWizyty) {
		this.idWizyty = idWizyty;
	}

	public String getSymbolTrasy() {
		return getString(symbolTrasy,"");
	}
	
	public void setSymbolTrasy(String symbolTrasy) {
		this.symbolTrasy = symbolTrasy;
	}
	
	public String getPunktWlasnySymbol() {
		return getString(punktWlasnySymbol,"");
	}	
	
	public String getPunktWlasnyNazwa() {
		return getString(punktWlasnyNazwa, "");
	}

	public void setPunktWlasnyNazwa(String punktWlasnyNazwa) {
		this.punktWlasnyNazwa = punktWlasnyNazwa;
	}

	public void setPunktWlasnySymbol(String punktWlasnySymbol) {
		this.punktWlasnySymbol = punktWlasnySymbol;
	}

	@JsonSerialize(using = JsonDateSerializer.class)
	public Date getMinCzasRozpoczeciaWgTrasy() {
		return minCzasRozpoczeciaWgTrasy;
	}
	
	public void setMinCzasRozpoczeciaWgTrasy(Date minCzasRozpoczeciaWgTrasy) {
		this.minCzasRozpoczeciaWgTrasy = minCzasRozpoczeciaWgTrasy;
	}

	@JsonSerialize(using = JsonDateSerializer.class)
	public Date getMaxCzasRozpoczeciaWgTrasy() {
		return maxCzasRozpoczeciaWgTrasy;
	}
	
	public void setMaxCzasRozpoczeciaWgTrasy(Date maxCzasRozpoczeciaWgTrasy) {
		this.maxCzasRozpoczeciaWgTrasy = maxCzasRozpoczeciaWgTrasy;
	}

	@JsonSerialize(using = JsonDateSerializer.class)
	public Date getMinCzasRozpoczeciaWgWizyty() {
		return minCzasRozpoczeciaWgWizyty;
	}
	
	public void setMinCzasRozpoczeciaWgWizyty(Date minCzasRozpoczeciaWgWizyty) {
		this.minCzasRozpoczeciaWgWizyty = minCzasRozpoczeciaWgWizyty;
	}

	@JsonSerialize(using = JsonDateSerializer.class)
	public Date getMaxCzasRozpoczeciaWgWizyty() {
		return maxCzasRozpoczeciaWgWizyty;
	}
	
	public void setMaxCzasRozpoczeciaWgWizyty(Date maxCzasRozpoczeciaWgWizyty) {
		this.maxCzasRozpoczeciaWgWizyty = maxCzasRozpoczeciaWgWizyty;
	}

	public String getNumerRejestracyjnyAuta() {
		return getString( numerRejestracyjnyAuta, "");
	}

	public void setNumerRejestracyjnyAuta(String numerRejestracyjnyAuta) {
		this.numerRejestracyjnyAuta = numerRejestracyjnyAuta;
	}

	public Boolean getCzasKrytyczny() {
		return null == czasKrytyczny ? false : czasKrytyczny;
	}

	public void setCzasKrytyczny(Boolean czasKrytyczny) {
		this.czasKrytyczny = czasKrytyczny;
	}

	public Integer getDystans() {
		return null == dystans ? 0 : dystans;
	}

	public void setDystans(Integer dystans) {
		this.dystans = dystans;
	}

	public Integer getIndeksWizyty() {
		return indeksWizyty;
	}

	public void setIndeksWizyty(Integer indeksWizyty) {
		this.indeksWizyty = indeksWizyty;
	}

	@Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final WizytaDaneWSDTO other = (WizytaDaneWSDTO) obj;
        if (this.idWizyty != other.idWizyty && (this.idWizyty == null || !this.idWizyty.equals(other.idWizyty))) {
            return false;
        }
        if ((this.symbolTrasy == null) ? (other.symbolTrasy != null) : !this.symbolTrasy.equals(other.symbolTrasy)) {
            return false;
        }
        if ((this.punktWlasnySymbol == null) ? (other.punktWlasnySymbol != null) : !this.punktWlasnySymbol.equals(other.punktWlasnySymbol)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + (this.idWizyty != null ? this.idWizyty.hashCode() : 0);
        hash = 83 * hash + (this.symbolTrasy != null ? this.symbolTrasy.hashCode() : 0);
        hash = 83 * hash + (this.punktWlasnySymbol != null ? this.punktWlasnySymbol.hashCode() : 0);
        return hash;
    }

	private String getString(String param, String result) {
		return (null == param || param.trim().isEmpty()) ? result : param;
	}
}
