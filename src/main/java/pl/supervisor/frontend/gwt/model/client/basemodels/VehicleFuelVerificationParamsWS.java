package pl.supervisor.frontend.gwt.model.client.basemodels;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by norbertl on 26.10.16.
 */
public class VehicleFuelVerificationParamsWS implements Serializable{

    private boolean pojazd;
    private double normJazda;
    private double normPostoj;
    private int sposobPomiaruPaliwa;
    private int bak1;
    private int bak2;
    private double czulosc;
    private boolean wygladzanie;
    private double martwaGora;
    private double martwyDol;
    private double k1;
    private double k2;
    private double korektaProcent;
    private double korektaLitry;
    private double zMinProcent;
    private int p2procent;

    private boolean newGenerationRecorder = false;
    private boolean onlyGuards = false;
    private Date baseFrom;
    private Date baseTo;

    public boolean isPojazd() {
        return pojazd;
    }

    public void setPojazd(boolean pojazd) {
        this.pojazd = pojazd;
    }

    public double getNormJazda() {
        return normJazda;
    }

    public void setNormJazda(double normJazda) {
        this.normJazda = normJazda;
    }

    public double getNormPostoj() {
        return normPostoj;
    }

    public void setNormPostoj(double normPostoj) {
        this.normPostoj = normPostoj;
    }

    public int getSposobPomiaruPaliwa() {
        return sposobPomiaruPaliwa;
    }

    public void setSposobPomiaruPaliwa(int sposobPomiaruPaliwa) {
        this.sposobPomiaruPaliwa = sposobPomiaruPaliwa;
    }

    public int getBak1() {
        return bak1;
    }

    public void setBak1(int bak1) {
        this.bak1 = bak1;
    }

    public int getBak2() {
        return bak2;
    }

    public void setBak2(int bak2) {
        this.bak2 = bak2;
    }

    public double getCzulosc() {
        return czulosc;
    }

    public void setCzulosc(double czulosc) {
        this.czulosc = czulosc;
    }

    public boolean isWygladzanie() {
        return wygladzanie;
    }

    public void setWygladzanie(boolean wygladzanie) {
        this.wygladzanie = wygladzanie;
    }

    public double getMartwaGora() {
        return martwaGora;
    }

    public void setMartwaGora(double martwaGora) {
        this.martwaGora = martwaGora;
    }

    public double getMartwyDol() {
        return martwyDol;
    }

    public void setMartwyDol(double martwyDol) {
        this.martwyDol = martwyDol;
    }

    public double getK1() {
        return k1;
    }

    public void setK1(double k1) {
        this.k1 = k1;
    }

    public double getK2() {
        return k2;
    }

    public void setK2(double k2) {
        this.k2 = k2;
    }

    public double getKorektaProcent() {
        return korektaProcent;
    }

    public void setKorektaProcent(double korektaProcent) {
        this.korektaProcent = korektaProcent;
    }

    public double getKorektaLitry() {
        return korektaLitry;
    }

    public void setKorektaLitry(double korektaLitry) {
        this.korektaLitry = korektaLitry;
    }

    public double getzMinProcent() {
        return zMinProcent;
    }

    public void setzMinProcent(double zMinProcent) {
        this.zMinProcent = zMinProcent;
    }

    public int getP2procent() {
        return p2procent;
    }

    public void setP2procent(int p2procent) {
        this.p2procent = p2procent;
    }

    public boolean isNewGenerationRecorder() {
        return newGenerationRecorder;
    }

    public void setNewGenerationRecorder(boolean newGenerationRecorder) {
        this.newGenerationRecorder = newGenerationRecorder;
    }

    public boolean isOnlyGuards() {
        return onlyGuards;
    }

    public void setOnlyGuards(boolean onlyGuards) {
        this.onlyGuards = onlyGuards;
    }

    public Date getBaseFrom() {
        return baseFrom;
    }

    public void setBaseFrom(Date baseFrom) {
        this.baseFrom = baseFrom;
    }

    public Date getBaseTo() {
        return baseTo;
    }

    public void setBaseTo(Date baseTo) {
        this.baseTo = baseTo;
    }
}
