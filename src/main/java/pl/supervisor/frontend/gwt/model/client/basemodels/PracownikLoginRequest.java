package pl.supervisor.frontend.gwt.model.client.basemodels;

import java.io.Serializable;

/**
 * Created by norbertl on 04.04.16.
 */
public class PracownikLoginRequest implements Serializable{

    private String login;
    private String password;
    private String licence;
    private String version;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLicence() {
        return licence;
    }

    public void setLicence(String licence) {
        this.licence = licence;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PracownikLoginRequest{");
        sb.append("login=").append(login);
        sb.append(", password='").append(password).append('\'');
        sb.append(", licence='").append(licence).append('\'');
        sb.append(", version='").append(version).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
