package pl.supervisor.frontend.gwt.model.client.basemodels;

import java.io.Serializable;

/**
 * Created by norbertl on 04.04.16.
 */
public class PracownikLoginResponse implements Serializable{

    private Boolean passwordVerification;
    private Boolean newAccount;

    public Boolean getPasswordVerification() {
        return passwordVerification;
    }

    public void setPasswordVerification(Boolean passwordVerification) {
        this.passwordVerification = passwordVerification;
    }

    public Boolean getNewAccount() {
        return newAccount;
    }

    public void setNewAccount(Boolean newAccount) {
        this.newAccount = newAccount;
    }

    public PracownikLoginResponse() {

        passwordVerification = false;
        newAccount = false;
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PracownikLoginResponse{");
        sb.append("passwordVerification=").append(passwordVerification);
        sb.append(", newAccount=").append(newAccount);
        sb.append('}');
        return sb.toString();
    }
}
