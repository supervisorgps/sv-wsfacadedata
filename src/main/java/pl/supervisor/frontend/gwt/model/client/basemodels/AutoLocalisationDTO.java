package pl.supervisor.frontend.gwt.model.client.basemodels;

import org.codehaus.jackson.annotate.JsonIgnore;

import java.io.Serializable;
import java.util.List;

/**
 * Created by abarczewski on 2015-01-28.
 */
public class AutoLocalisationDTO implements Serializable {

    @JsonIgnore
    private static final long serialVersionUID = 1706894783919487978L;

    private String registrationNumber;
    private String symbol;
    private Double longitude;
    private Double latitude;
    private String driver;
    private Integer litersOfFuel;
    private String countryName;
    private String province;
    private String city;
    private String street;
    private String zipCode;
    private String routeNumber;
    private String picket;
    private Integer speed;
    private Boolean isEngineRunning;
    private String timePoint;
    private List<String> groupNames;
    private Short powerBattery;
    private Boolean isActive;
    private Double temp1;
    private Double temp2;
    private Double temp3;

    public AutoLocalisationDTO() {
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public Integer getLitersOfFuel() {
        return litersOfFuel;
    }

    public void setLitersOfFuel(Integer litersOfFuel) {
        this.litersOfFuel = litersOfFuel;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getRouteNumber() {
        return routeNumber;
    }

    public void setRouteNumber(String routeNumber) {
        this.routeNumber = routeNumber;
    }

    public String getPicket() {
        return picket;
    }

    public void setPicket(String picket) {
        this.picket = picket;
    }

    public Integer getSpeed() {
        return speed;
    }

    public void setSpeed(Integer speed) {
        this.speed = speed;
    }

    public Boolean getIsEngineRunning() {
        return isEngineRunning;
    }

    public void setIsEngineRunning(Boolean isEngineRunning) {
        this.isEngineRunning = isEngineRunning;
    }

    public String getTimePoint() {
        return timePoint;
    }

    public void setTimePoint(String timePoint) {
        this.timePoint = timePoint;
    }

    public List<String> getGroupNames() {
        return groupNames;
    }

    public void setGroupNames(List<String> groupNames) {
        this.groupNames = groupNames;
    }

    public Short getPowerBattery() {
        return powerBattery;
    }

    public void setPowerBattery(Short powerBattery) {
        this.powerBattery = powerBattery;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Double getTemp1() {
        return temp1;
    }

    public void setTemp1(Double temp1) {
        this.temp1 = temp1;
    }

    public Double getTemp2() {
        return temp2;
    }

    public void setTemp2(Double temp2) {
        this.temp2 = temp2;
    }

    public Double getTemp3() {
        return temp3;
    }

    public void setTemp3(Double temp3) {
        this.temp3 = temp3;
    }
}
