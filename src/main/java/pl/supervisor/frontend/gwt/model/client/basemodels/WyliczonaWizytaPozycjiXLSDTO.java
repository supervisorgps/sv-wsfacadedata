package pl.supervisor.frontend.gwt.model.client.basemodels;

import java.util.Date;

public class WyliczonaWizytaPozycjiXLSDTO {

	private Long wyliczonaWizytaId;
	
	private Date wyliczonaWizytaStart;
	private Date wyliczonaWizytaEnd;
	
	private Integer klientId;
	
	private String autoDaneNrRejestracyjny;
	
	private String punktWlasnySymbol;
	
	private Integer wyliczonaWizytaU1sec;
	private Integer wyliczonaWizytaU2sec;
	private Integer wyliczonaWizytaU3sec;
	private Integer wyliczonaWizytaU4sec;
	
	private String nazwaKlienta;
	
	public WyliczonaWizytaPozycjiXLSDTO(){}

	public WyliczonaWizytaPozycjiXLSDTO(Long wyliczonaWizytaId,
			Date wyliczonaWizytaStart, Date wyliczonaWizytaEnd, Integer klientId,
			String autoDaneNrRejestracyjny, String punktWlasnySymbol,
			Integer wyliczonaWizytaU1sec, Integer wyliczonaWizytaU2sec,
			Integer wyliczonaWizytaU3sec, Integer wyliczonaWizytaU4sec,
			String nazwaKlienta) {
		
		this.wyliczonaWizytaId = wyliczonaWizytaId;
		this.wyliczonaWizytaStart = wyliczonaWizytaStart;	
		this.wyliczonaWizytaEnd = wyliczonaWizytaEnd;
		this.klientId = klientId;
		this.autoDaneNrRejestracyjny = autoDaneNrRejestracyjny;
		this.punktWlasnySymbol = punktWlasnySymbol;
		this.wyliczonaWizytaU1sec = wyliczonaWizytaU1sec;
		this.wyliczonaWizytaU2sec = wyliczonaWizytaU2sec;
		this.wyliczonaWizytaU3sec = wyliczonaWizytaU3sec;
		this.wyliczonaWizytaU4sec = wyliczonaWizytaU4sec;
		this.nazwaKlienta = nazwaKlienta;
	}
	
	public Long getWyliczonaWizytaId() {
		return wyliczonaWizytaId;
	}
	public Date getWyliczonaWizytaStart() {
		return wyliczonaWizytaStart;
	}
	public Date getWyliczonaWizytaEnd() {
		return wyliczonaWizytaEnd;
	}
	public Integer getKlientId() {
		return klientId;
	}
	public String getAutoDaneNrRejestracyjny() {
		return autoDaneNrRejestracyjny;
	}
	public String getPunktWlasnySymbol() {
		return punktWlasnySymbol;
	}
	public Integer getWyliczonaWizytaU1sec() {
		return wyliczonaWizytaU1sec;
	}
	public Integer getWyliczonaWizytaU2sec() {
		return wyliczonaWizytaU2sec;
	}
	public Integer getWyliczonaWizytaU3sec() {
		return wyliczonaWizytaU3sec;
	}
	public Integer getWyliczonaWizytaU4sec() {
		return wyliczonaWizytaU4sec;
	}

	public void setWyliczonaWizytaId(Long wyliczonaWizytaId) {
		this.wyliczonaWizytaId = wyliczonaWizytaId;
	}

	public void setWyliczonaWizytaStart(Date wyliczonaWizytaStart) {
		this.wyliczonaWizytaStart = wyliczonaWizytaStart;
	}

	public void setWyliczonaWizytaEnd(Date wyliczonaWizytaEnd) {
		this.wyliczonaWizytaEnd = wyliczonaWizytaEnd;
	}

	public void setKlientId(Integer klientId) {
		this.klientId = klientId;
	}

	public void setAutoDaneNrRejestracyjny(String autoDaneNrRejestracyjny) {
		this.autoDaneNrRejestracyjny = autoDaneNrRejestracyjny;
	}

	public void setPunktWlasnySymbol(String punktWlasnySymbol) {
		this.punktWlasnySymbol = punktWlasnySymbol;
	}

	public void setWyliczonaWizytaU1sec(Integer wyliczonaWizytaU1sec) {
		this.wyliczonaWizytaU1sec = wyliczonaWizytaU1sec;
	}

	public void setWyliczonaWizytaU2sec(Integer wyliczonaWizytaU2sec) {
		this.wyliczonaWizytaU2sec = wyliczonaWizytaU2sec;
	}

	public void setWyliczonaWizytaU3sec(Integer wyliczonaWizytaU3sec) {
		this.wyliczonaWizytaU3sec = wyliczonaWizytaU3sec;
	}

	public void setWyliczonaWizytaU4sec(Integer wyliczonaWizytaU4sec) {
		this.wyliczonaWizytaU4sec = wyliczonaWizytaU4sec;
	}

	public String getNazwaKlienta() {
		return nazwaKlienta;
	}

	public void setNazwaKlienta(String nazwaKlienta) {
		this.nazwaKlienta = nazwaKlienta;
	}
		
}
