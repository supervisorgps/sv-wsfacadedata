package pl.supervisor.frontend.gwt.model.client.basemodels;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by norbertl on 25.10.16.
 */
public class FuelAnalysisWSResult implements Serializable{

    private List<TankowanieFakturaWSResult> tankowaniaFaktury = new ArrayList<TankowanieFakturaWSResult>();
    private Date from;
    private Date to;
    private double p2litry;
    private FuelSummaryWS podsumowanie = new FuelSummaryWS();
    private VehicleFuelVerificationParamsWS vehicleParams = new VehicleFuelVerificationParamsWS();

    public List<TankowanieFakturaWSResult> getTankowaniaFaktury() {
        return tankowaniaFaktury;
    }

    public void setTankowaniaFaktury(List<TankowanieFakturaWSResult> tankowaniaFaktury) {
        this.tankowaniaFaktury = tankowaniaFaktury;
    }

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }

    public double getP2litry() {
        return p2litry;
    }

    public void setP2litry(double p2litry) {
        this.p2litry = p2litry;
    }

    public FuelSummaryWS getPodsumowanie() {
        return podsumowanie;
    }

    public void setPodsumowanie(FuelSummaryWS podsumowanie) {
        this.podsumowanie = podsumowanie;
    }

    public VehicleFuelVerificationParamsWS getVehicleParams() {
        return vehicleParams;
    }

    public void setVehicleParams(VehicleFuelVerificationParamsWS vehicleParams) {
        this.vehicleParams = vehicleParams;
    }
}
