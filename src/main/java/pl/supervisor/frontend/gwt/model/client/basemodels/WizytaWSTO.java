package pl.supervisor.frontend.gwt.model.client.basemodels;

import org.codehaus.jackson.annotate.JsonIgnore;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="visit", namespace="sv.integration.ws.route")
public class WizytaWSTO implements Serializable {
	
	@XmlElement(required=false)
	private Long id;		// id generowane przez SV
	
	@XmlElement(required=true)
	private String externalId;	// zewnętrzne ID - w przypadku CDG guid wyjazdu
	
	@XmlElement(required=true)
	private String locationExternalId1; // zewnętrzne ID miejsca wizyty (miejsca własnego) - w przypadku CDG guid płatnika
	
	@XmlElement(required=true)
	private String locationExternalId2; // zewnętrzne ID miejsca wizyty (miejsca własnego) - w przypadku CDG guid klienta
	
	@XmlElement(required=true)
	private String locationExternalId3; // zewnętrzne ID miejsca wizyty (miejsca własnego) - w przypadku CDG guid lokalizacji
	
	@XmlElement(required=true)
	private String locationName;	// nazwa miejsca wizyty (miejsca własnego) 
	
	@XmlElement(required=true)
	private String locationSymbol;	// symbol miejsca wizyty (miejsca własnego)
	
	@XmlElement(required=false)
	private String locationCountry;	// kraj miejsca wizyty (miejsca własnego)
	
	@XmlElement(required=false)
	private String locationArea;    // województwo miejsca wizyty (miejsca własnego)
	
	@XmlElement(required=false)
	private String locationCity;    // miejscowość miejsca wizyty (miejsca własnego)
	
	@XmlElement(required=false)
	private String locationZipCode;	// kod pocztowy miejsca wizyty (miejsca własnego)
	
	@XmlElement(required=false)
	private String locationStreet;	// ulica miejsca wizyty (miejsca własnego)
	
	@XmlElement(required=false)
	private String locationNumber;  // nr posesji miejsca wizyty (miejsca własnego)
	
	@XmlElement(required=false)
	private Double locationLongitude; 	// długość geograficzna miejsca wizyty (miejsca własnego)
	
	@XmlElement(required=false)
	private Double locationLatitude; 	// szerokość geograficzna miejsca wizyty (miejsca własnego)
	
	@XmlElement(required=false)
	private Integer locationRadius;			// promień (wokół lon/lat)miejsca wizyty (miejsca własnego)
	
	@XmlElement(required=false)
	private Date visitDateTime;		// data / czas wizyty - wymuszona

	@XmlElement(required=false)
	private Date possibleVisitFromDateTime;		// ramy czasowe - kiedy może nastąpić dostawa (początek)
	
	@XmlElement(required=false)
	private Date possibleVisitToDateTime;	// ramy czasowe - kiedy może nastąpić dostawa (koniec)
	
	@XmlElement(required=true)
	private Integer visitDuration;	// czas trwania wizyty / obsługi punktu dla CDG
	
	@XmlElement(required=false)
	private Integer routeOrderIndex;	// numer wizyty w kolejności
	
	@XmlElement(required=false)
	private Integer currentRouteMileage;	// aktualny przebieg trasy - ilość km z trasy w bieżącej wizycie

	@XmlElement(required=true)
	private Boolean startPoint;
	
	@XmlElement(required=true)
	private Boolean endPoint;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public String getLocationExternalId1() {
		return locationExternalId1;
	}

	public void setLocationExternalId1(String locationExternalId1) {
		this.locationExternalId1 = locationExternalId1;
	}

	public String getLocationExternalId2() {
		return locationExternalId2;
	}

	public void setLocationExternalId2(String locationExternalId2) {
		this.locationExternalId2 = locationExternalId2;
	}

	public String getLocationExternalId3() {
		return locationExternalId3;
	}

	public void setLocationExternalId3(String locationExternalId3) {
		this.locationExternalId3 = locationExternalId3;
	}

	public String getLocationCountry() {
		return locationCountry;
	}

	public void setLocationCountry(String locationCountry) {
		this.locationCountry = locationCountry;
	}

	public String getLocationArea() {
		return locationArea;
	}

	public void setLocationArea(String locationArea) {
		this.locationArea = locationArea;
	}

	public String getLocationCity() {
		return locationCity;
	}

	public void setLocationCity(String locationCity) {
		this.locationCity = locationCity;
	}

	public String getLocationZipCode() {
		return locationZipCode;
	}

	public void setLocationZipCode(String locationZipCode) {
		this.locationZipCode = locationZipCode;
	}

	public String getLocationStreet() {
		return locationStreet;
	}

	public void setLocationStreet(String locationStreet) {
		this.locationStreet = locationStreet;
	}

	public String getLocationNumber() {
		return locationNumber;
	}

	public void setLocationNumber(String locationNumber) {
		this.locationNumber = locationNumber;
	}

	public Double getLocationLongitude() {
		return locationLongitude;
	}

	public void setLocationLongitude(Double locationLongitude) {
		this.locationLongitude = locationLongitude;
	}

	public Double getLocationLatitude() {
		return locationLatitude;
	}

	public void setLocationLatitude(Double locationLatitude) {
		this.locationLatitude = locationLatitude;
	}

	public Date getVisitDateTime() {
		return visitDateTime;
	}

	@JsonIgnore
	public int getVisitDateTimeAsMinutes(){
		
		return visitDateTime.getHours() * 60 + visitDateTime.getMinutes(); 
	}

	public void setVisitDateTime(Date visitDateTime) {
		this.visitDateTime = visitDateTime;
	}

	public Date getPossibleVisitFromDateTime() {
		return possibleVisitFromDateTime;
	}

	@JsonIgnore
	public int getPossibleVisitFromDateTimeAsMinutes() {

		if (null == possibleVisitFromDateTime) {

			return -1; // brak okna
		}

		return possibleVisitFromDateTime.getHours() * 60 + possibleVisitFromDateTime.getMinutes();
	}

	public void setPossibleVisitFromDateTime(Date possibleVisitFromDateTime) {
		this.possibleVisitFromDateTime = possibleVisitFromDateTime;
	}

	public Date getPossibleVisitToDateTime() {
		return possibleVisitToDateTime;
	}

	@JsonIgnore
	public int getPossibleVisitToDateTimeAsMinutes() {

		if (null == possibleVisitToDateTime) {

			return -1; // brak okna
		}

		return possibleVisitToDateTime.getHours() * 60 + possibleVisitToDateTime.getMinutes();
	}
	
	public void setPossibleVisitToDateTime(Date possibleVisitToDateTime) {
		this.possibleVisitToDateTime = possibleVisitToDateTime;
	}

	public Integer getCurrentRouteMileage() {
		return currentRouteMileage;
	}

	public void setCurrentRouteMileage(Integer currentRouteMileage) {
		this.currentRouteMileage = currentRouteMileage;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public String getLocationSymbol() {
		return locationSymbol;
	}

	public void setLocationSymbol(String locationSymbol) {
		this.locationSymbol = locationSymbol;
	}

	public Integer getLocationRadius() {
		return locationRadius;
	}

	public void setLocationRadius(Integer locationRadius) {
		this.locationRadius = locationRadius;
	}

	public Integer getVisitDuration() {
		return visitDuration;
	}

	public void setVisitDuration(Integer visitDuration) {
		this.visitDuration = visitDuration;
	}

	public Integer getRouteOrderIndex() {
		return routeOrderIndex;
	}

	public void setRouteOrderIndex(Integer routeOrderIndex) {
		this.routeOrderIndex = routeOrderIndex;
	}

	public Boolean getStartPoint() {
		return startPoint;
	}

	public void setStartPoint(Boolean startPoint) {
		this.startPoint = startPoint;
	}

	public Boolean getEndPoint() {
		return endPoint;
	}

	public void setEndPoint(Boolean endPoint) {
		this.endPoint = endPoint;
	}

	@JsonIgnore
	public boolean checkCoords(){

		boolean coordsOk = true;

		//
		if ( null == getLocationLatitude() || null == getLocationLongitude()){

			coordsOk = false;
		}
		//

		return coordsOk;
	}
}
