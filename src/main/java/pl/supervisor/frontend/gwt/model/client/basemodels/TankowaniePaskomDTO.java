package pl.supervisor.frontend.gwt.model.client.basemodels;

import org.codehaus.jackson.annotate.JsonIgnore;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by abarczewski on 2015-01-14.
 */
public class TankowaniePaskomDTO implements Serializable {

    @JsonIgnore
    private static final long serialVersionUID = -5606842333916087978L;

    @NotNull
    private String idCost;
    @NotNull
    private String autoRegisterNumber;
    @NotNull
    private String invoiceDate;
    @NotNull
    private int countLiters;

    public TankowaniePaskomDTO() {
    }

    public TankowaniePaskomDTO(String idCost, String autoRegisterNumber, String invoiceDate, int countLiters) {
        this.idCost = idCost;
        this.autoRegisterNumber = autoRegisterNumber;
        this.invoiceDate = invoiceDate;
        this.countLiters = countLiters;
    }

    public String getIdCost() {
        return idCost;
    }

    public void setIdCost(String idCost) {
        this.idCost = idCost;
    }

    public String getAutoRegisterNumber() {
        return autoRegisterNumber;
    }

    public void setAutoRegisterNumber(String autoRegisterNumber) {
        this.autoRegisterNumber = autoRegisterNumber;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public int getCountLiters() {
        return countLiters;
    }

    public void setCountLiters(int countLiters) {
        this.countLiters = countLiters;
    }

    @Override
    public String toString() {
        return "TankowaniePaskomDTO{" +
                "idCost='" + idCost + '\'' +
                ", autoRegisterNumber='" + autoRegisterNumber + '\'' +
                ", invoiceDate='" + invoiceDate + '\'' +
                ", countLiters=" + countLiters +
                '}';
    }

    public String toIdImport() {
        return idCost + "," + autoRegisterNumber + "," + invoiceDate + "," + countLiters;
    }
}
