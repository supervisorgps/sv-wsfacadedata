package pl.supervisor.frontend.gwt.model.client.exception;


public class LoginException extends BackendException {

	public LoginException(String string) {
		super(string);
	}

	public LoginException(){}
}
