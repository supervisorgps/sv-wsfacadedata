package pl.supervisor.frontend.gwt.model.client.basemodels;

import java.io.Serializable;

public class FuelSummaryWS implements Serializable{

    private Integer dystansM;
    private String licznikPoczatek;
    private String licznikKoniec;
    private Integer czasPostojow;
    private Integer czasJazd;
    private Integer stanBakuPoczatek;
    private Integer stanBakuKoniec;
    private Double zuzycieNormJazda;
    private Double zuzycieNormPostoj;
    private Integer czasPracy;
    private Integer fakturyIlosc;
    private Double fakturyLitry;
    private Double fakturyZuzycieRuch;
    private Double fakturyZuzycie1h;
    private Double fakturyZuzycie;
    private Integer tankowaniaIlosc;
    private Double tankowaniaLitry;
    private Integer upustyIlosc;
    private Double upustyLitry;
    private Double tankowaniaZuzycieRuch;
    private Double tankowaniaZuzycieRuchCan;
    private Double tankowaniaZuzycie1h;
    private Double tankowaniaZuzycie1hCan;
    private Double tankowaniaZuzycie;
    private Double canZuzycie;
    private Integer dziury;
    private Integer teleporty;
    private Integer workingSeconds;
    private Double zuzycieAgregatDevice;

    public Integer getDystansM() {
        return dystansM;
    }

    public void setDystansM(Integer dystansM) {
        this.dystansM = dystansM;
    }

    public String getLicznikPoczatek() {
        return licznikPoczatek;
    }

    public void setLicznikPoczatek(String licznikPoczatek) {
        this.licznikPoczatek = licznikPoczatek;
    }

    public String getLicznikKoniec() {
        return licznikKoniec;
    }

    public void setLicznikKoniec(String licznikKoniec) {
        this.licznikKoniec = licznikKoniec;
    }

    public Integer getCzasPostojow() {
        return czasPostojow;
    }

    public void setCzasPostojow(Integer czasPostojow) {
        this.czasPostojow = czasPostojow;
    }

    public Integer getCzasJazd() {
        return czasJazd;
    }

    public void setCzasJazd(Integer czasJazd) {
        this.czasJazd = czasJazd;
    }

    public Integer getStanBakuPoczatek() {
        return stanBakuPoczatek;
    }

    public void setStanBakuPoczatek(Integer stanBakuPoczatek) {
        this.stanBakuPoczatek = stanBakuPoczatek;
    }

    public Integer getStanBakuKoniec() {
        return stanBakuKoniec;
    }

    public void setStanBakuKoniec(Integer stanBakuKoniec) {
        this.stanBakuKoniec = stanBakuKoniec;
    }

    public Double getZuzycieNormJazda() {
        return zuzycieNormJazda;
    }

    public void setZuzycieNormJazda(Double zuzycieNormJazda) {
        this.zuzycieNormJazda = zuzycieNormJazda;
    }

    public Double getZuzycieNormPostoj() {
        return zuzycieNormPostoj;
    }

    public void setZuzycieNormPostoj(Double zuzycieNormPostoj) {
        this.zuzycieNormPostoj = zuzycieNormPostoj;
    }

    public Integer getCzasPracy() {
        return czasPracy;
    }

    public void setCzasPracy(Integer czasPracy) {
        this.czasPracy = czasPracy;
    }

    public Integer getFakturyIlosc() {
        return fakturyIlosc;
    }

    public void setFakturyIlosc(Integer fakturyIlosc) {
        this.fakturyIlosc = fakturyIlosc;
    }

    public Double getFakturyLitry() {
        return fakturyLitry;
    }

    public void setFakturyLitry(Double fakturyLitry) {
        this.fakturyLitry = fakturyLitry;
    }

    public Double getFakturyZuzycieRuch() {
        return fakturyZuzycieRuch;
    }

    public void setFakturyZuzycieRuch(Double fakturyZuzycieRuch) {
        this.fakturyZuzycieRuch = fakturyZuzycieRuch;
    }

    public Double getFakturyZuzycie1h() {
        return fakturyZuzycie1h;
    }

    public void setFakturyZuzycie1h(Double fakturyZuzycie1h) {
        this.fakturyZuzycie1h = fakturyZuzycie1h;
    }

    public Double getFakturyZuzycie() {
        return fakturyZuzycie;
    }

    public void setFakturyZuzycie(Double fakturyZuzycie) {
        this.fakturyZuzycie = fakturyZuzycie;
    }

    public Integer getTankowaniaIlosc() {
        return tankowaniaIlosc;
    }

    public void setTankowaniaIlosc(Integer tankowaniaIlosc) {
        this.tankowaniaIlosc = tankowaniaIlosc;
    }

    public Double getTankowaniaLitry() {
        return tankowaniaLitry;
    }

    public void setTankowaniaLitry(Double tankowaniaLitry) {
        this.tankowaniaLitry = tankowaniaLitry;
    }

    public Integer getUpustyIlosc() {
        return upustyIlosc;
    }

    public void setUpustyIlosc(Integer upustyIlosc) {
        this.upustyIlosc = upustyIlosc;
    }

    public Double getUpustyLitry() {
        return upustyLitry;
    }

    public void setUpustyLitry(Double upustyLitry) {
        this.upustyLitry = upustyLitry;
    }

    public Double getTankowaniaZuzycieRuch() {
        return tankowaniaZuzycieRuch;
    }

    public void setTankowaniaZuzycieRuch(Double tankowaniaZuzycieRuch) {
        this.tankowaniaZuzycieRuch = tankowaniaZuzycieRuch;
    }

    public Double getTankowaniaZuzycieRuchCan() {
        return tankowaniaZuzycieRuchCan;
    }

    public void setTankowaniaZuzycieRuchCan(Double tankowaniaZuzycieRuchCan) {
        this.tankowaniaZuzycieRuchCan = tankowaniaZuzycieRuchCan;
    }

    public Double getTankowaniaZuzycie1h() {
        return tankowaniaZuzycie1h;
    }

    public void setTankowaniaZuzycie1h(Double tankowaniaZuzycie1h) {
        this.tankowaniaZuzycie1h = tankowaniaZuzycie1h;
    }

    public Double getTankowaniaZuzycie1hCan() {
        return tankowaniaZuzycie1hCan;
    }

    public void setTankowaniaZuzycie1hCan(Double tankowaniaZuzycie1hCan) {
        this.tankowaniaZuzycie1hCan = tankowaniaZuzycie1hCan;
    }

    public Double getTankowaniaZuzycie() {
        return tankowaniaZuzycie;
    }

    public void setTankowaniaZuzycie(Double tankowaniaZuzycie) {
        this.tankowaniaZuzycie = tankowaniaZuzycie;
    }

    public Double getCanZuzycie() {
        return canZuzycie;
    }

    public void setCanZuzycie(Double canZuzycie) {
        this.canZuzycie = canZuzycie;
    }

    public Integer getDziury() {
        return dziury;
    }

    public void setDziury(Integer dziury) {
        this.dziury = dziury;
    }

    public Integer getTeleporty() {
        return teleporty;
    }

    public void setTeleporty(Integer teleporty) {
        this.teleporty = teleporty;
    }

    public Integer getWorkingSeconds() {
        return workingSeconds;
    }

    public void setWorkingSeconds(Integer workingSeconds) {
        this.workingSeconds = workingSeconds;
    }

    public Double getZuzycieAgregatDevice() {
        return zuzycieAgregatDevice;
    }

    public void setZuzycieAgregatDevice(Double zuzycieAgregatDevice) {
        this.zuzycieAgregatDevice = zuzycieAgregatDevice;
    }
}
