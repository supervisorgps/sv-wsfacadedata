/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.supervisor.frontend.gwt.model.client.exception;

/**
 *
 * @author Administrator
 */
public class BackendException extends Exception {

	public static final int INCORECT_LOGIN_PASSWORD = 1;
	public static final int CLIENT_INACTIVE = 2;
	
	private int type;
	private String msg;
	
    public BackendException() {
    	setType(0);
    }

    public BackendException(int type) {
    	setType(type);
    }

    public BackendException(String string) {
        super(string);
    }

    

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public int getType() {
		return type;
	}


	public void setType(int type) {
		this.type = type;
	}
    
    
}
