package pl.supervisor.frontend.gwt.model.client.basemodels;

import java.util.Date;

/**
 * Klasa typu TO objemująca dane z o pozycji pojazdu
 * oraz lokalizacji (geopunkt)
 * Zawiera dane:
 * - z tabeli Pozycje
 * - z tabeli Geopunkt
 * - z tabeli AutoDane (nr_rejestracyjny)
 *
 * Stworzona w trakcie realizacji zamówienia na usługę
 * udostępniającą pozycje poprzez REST dla firmy BOTAM.
 *
 * @author norbertl
 *
 */
public class PozycjeWSDTO {

    private Long positionId;
    private String registrationNumber;
    private Date positionDateTime;
    private Integer speed;
    private Integer driverId;
    private Integer fuelLevel;

    private Boolean gps;
    private Boolean engine;

    private Double longitude;
    private Double latitude;

    private String street;
    private String country;
    private String city;

    private String poi1;

    private Double temperature1;
    private Double temperature2;
    private Double temperature3;

    private Double voltage;


    public PozycjeWSDTO() {
    }

    public PozycjeWSDTO(Long positionId, Long vehicleId, Date positionDateTime,
                        Integer speed, Integer driverId, Integer fuelLevel, Boolean gps,
                        Boolean engine,  Double longitude, Double latitude, String street,
                        String country, String city) {

        this.positionId = positionId;
        this.registrationNumber = vehicleId != null ? vehicleId.toString() : null;
        this.positionDateTime = positionDateTime;
        this.speed = speed;
        this.driverId = driverId;
        this.fuelLevel = fuelLevel;
        this.gps = gps;
        this.engine = engine;
        this.longitude = longitude;
        this.latitude = latitude;
        this.street = street;
        this.country = country;
        this.city = city;
    }

    public Long getPositionId() {
        return positionId;
    }


    public void setPositionId(Long positionId) {
        this.positionId = positionId;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public Date getPositionDateTime() {
        return positionDateTime;
    }

    public void setPositionDateTime(Date positionDateTime) {
        this.positionDateTime = positionDateTime;
    }
    public Integer getSpeed() {
        return speed;
    }
    public void setSpeed(Integer speed) {
        this.speed = speed;
    }
    public Integer getDriverId() {
        return driverId;
    }
    public void setDriverId(Integer driverId) {
        this.driverId = driverId;
    }
    public Integer getFuelLevel() {
        return fuelLevel;
    }
    public void setFuelLevel(Integer fuelLevel) {
        this.fuelLevel = fuelLevel;
    }
    public Boolean getGps() {
        return gps;
    }
    public void setGps(Boolean gps) {
        this.gps = gps;
    }
    public Boolean getEngine() {
        return engine;
    }
    public void setEngine(Boolean engine) {
        this.engine = engine;
    }
    public Double getLongitude() {
        return longitude;
    }
    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }
    public Double getLatitude() {
        return latitude;
    }
    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }
    public String getStreet() {
        return street;
    }
    public void setStreet(String street) {
        this.street = street;
    }
    public String getCountry() {
        return country;
    }
    public void setCountry(String country) {
        this.country = country;
    }
    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }

    public String getPoi1() {
        return poi1;
    }

    public void setPoi1(String poi1) {
        this.poi1 = poi1;
    }

    @Override
    public String toString() {
        return "PozycjeWSDTO [positionId=" + positionId
                + ", registrationNumber=" + registrationNumber
                + ", positionDateTime=" + positionDateTime + ", speed=" + speed
                + ", driverId=" + driverId + ", fuelLevel=" + fuelLevel
                + ", gps=" + gps + ", engine=" + engine + ", longitude="
                + longitude + ", latitude=" + latitude + ", street=" + street
                + ", country=" + country + ", city=" + city + "]\n";
    }


    public Double getTemperature1() {
        return temperature1;
    }

    public void setTemperature1(Double temperature1) {
        this.temperature1 = temperature1;
    }

    public Double getTemperature2() {
        return temperature2;
    }

    public void setTemperature2(Double temperature2) {
        this.temperature2 = temperature2;
    }

    public Double getTemperature3() {
        return temperature3;
    }

    public void setTemperature3(Double temperature3) {
        this.temperature3 = temperature3;
    }

    public Double getVoltage() {
        return voltage;
    }

    public void setVoltage(Double voltage) {
        this.voltage = voltage;
    }
}
