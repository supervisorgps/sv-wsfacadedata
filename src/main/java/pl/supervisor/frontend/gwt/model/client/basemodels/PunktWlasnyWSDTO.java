package pl.supervisor.frontend.gwt.model.client.basemodels;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;

@XmlAccessorType(XmlAccessType.FIELD)
public class PunktWlasnyWSDTO implements Serializable {

    @XmlElement(required = true)
    private Long id;
    private String nazwa;
    private String symbol;
    @XmlElement(required = true)
    private Boolean aktywny;
    @XmlElement(required = true)
    private Boolean jednorazowy;

    private String opis;
    @XmlElement(required = true)
    private Short promien;

    private String krajWpis;
    private String wojewodztwoWpis;
    private String miastoWpis;
    private String ulicaWpis;
    private String nrPosesjiWpis;
    private String kodWpis;

    private String krajGeoserwer;
    private String wojewodztwoGeoserwer;
    private String miastoGeoserwer;
    private String ulicaGeoserwer;
    private String kodGeoserwer;

    @XmlElement(required = true)
    private Double longitude;
    @XmlElement(required = true)
    private Double latitude;

    @XmlElement(required = true)
    private Boolean zatwWspolrzedne;
    @XmlElement(required = true)
    private Boolean zatwMiasto;
    @XmlElement(required = true)
    private Boolean zatwUlica;
    @XmlElement(required = true)
    private Boolean zatwKod;

    private String pwlNrBudynku;
    private String pwlNrLokalu;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public boolean isAktywny() {
        return aktywny;
    }

    public void setAktywny(boolean aktywny) {
        this.aktywny = aktywny;
    }

    public boolean isJednorazowy() {
        return jednorazowy;
    }

    public void setJednorazowy(boolean jednorazowy) {
        this.jednorazowy = jednorazowy;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public Short getPromien() {
        return promien;
    }

    public void setPromien(Short promien) {
        this.promien = promien;
    }

    public String getKrajWpis() {
        return krajWpis;
    }

    public void setKrajWpis(String krajWpis) {
        this.krajWpis = krajWpis;
    }

    public String getWojewodztwoWpis() {
        return wojewodztwoWpis;
    }

    public void setWojewodztwoWpis(String wojewodztwoWpis) {
        this.wojewodztwoWpis = wojewodztwoWpis;
    }

    public String getMiastoWpis() {
        return miastoWpis;
    }

    public void setMiastoWpis(String miastoWpis) {
        this.miastoWpis = miastoWpis;
    }

    public String getUlicaWpis() {
        return ulicaWpis;
    }

    public void setUlicaWpis(String ulicaWpis) {
        this.ulicaWpis = ulicaWpis;
    }

    public String getNrPosesjiWpis() {
        return nrPosesjiWpis;
    }

    public void setNrPosesjiWpis(String nrPosesjiWpis) {
        this.nrPosesjiWpis = nrPosesjiWpis;
    }

    public String getKodWpis() {
        return kodWpis;
    }

    public void setKodWpis(String kodWpis) {
        this.kodWpis = kodWpis;
    }

    public String getKrajGeoserwer() {
        return krajGeoserwer;
    }

    public void setKrajGeoserwer(String krajGeoserwer) {
        this.krajGeoserwer = krajGeoserwer;
    }

    public String getWojewodztwoGeoserwer() {
        return wojewodztwoGeoserwer;
    }

    public void setWojewodztwoGeoserwer(String wojewodztwoGeoserwer) {
        this.wojewodztwoGeoserwer = wojewodztwoGeoserwer;
    }

    public String getMiastoGeoserwer() {
        return miastoGeoserwer;
    }

    public void setMiastoGeoserwer(String miastoGeoserwer) {
        this.miastoGeoserwer = miastoGeoserwer;
    }

    public String getUlicaGeoserwer() {
        return ulicaGeoserwer;
    }

    public void setUlicaGeoserwer(String ulicaGeoserwer) {
        this.ulicaGeoserwer = ulicaGeoserwer;
    }

    public String getKodGeoserwer() {
        return kodGeoserwer;
    }

    public void setKodGeoserwer(String kodGeoserwer) {
        this.kodGeoserwer = kodGeoserwer;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public Boolean getZatwWspolrzedne() {
        return zatwWspolrzedne;
    }

    public void setZatwWspolrzedne(Boolean zatwWspolrzedne) {
        this.zatwWspolrzedne = zatwWspolrzedne;
    }

    public Boolean getZatwMiasto() {
        return zatwMiasto;
    }

    public void setZatwMiasto(Boolean zatwMiasto) {
        this.zatwMiasto = zatwMiasto;
    }

    public Boolean getZatwUlica() {
        return zatwUlica;
    }

    public void setZatwUlica(Boolean zatwUlica) {
        this.zatwUlica = zatwUlica;
    }

    public Boolean getZatwKod() {
        return zatwKod;
    }

    public void setZatwKod(Boolean zatwKod) {
        this.zatwKod = zatwKod;
    }

    public String getPwlNrBudynku() {
        return pwlNrBudynku;
    }

    public void setPwlNrBudynku(String pwlNrBudynku) {
        this.pwlNrBudynku = pwlNrBudynku;
    }

    public String getPwlNrLokalu() {
        return pwlNrLokalu;
    }

    public void setPwlNrLokalu(String pwlNrLokalu) {
        this.pwlNrLokalu = pwlNrLokalu;
    }

    public PunktWlasnyWSDTO() {
    }
}
