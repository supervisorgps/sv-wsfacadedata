package pl.supervisor.frontend.gwt.model.client.basemodels;

import java.io.Serializable;

/**
 * Created by norbertl on 18.03.16.
 */
public class PracownikWSDTO implements Serializable {

    private Long idBazodanowe;
    private String imie;
    private String nazwisko;
    private char plec;
    private Boolean aktywny;
    private Boolean kierowca;
    private Boolean uzytkownikSystemuSV;
    private Integer identyfikatorSv;
    private Integer identyfikatorObcy;
    private String loginPda;
    private String nrTelefonu;

    public PracownikWSDTO() {
    }

    public PracownikWSDTO(Long idBazodanowe, String imie, String nazwisko, String plec, Boolean aktywny, Boolean kierowca, Boolean uzytkownikSystemuSV, Integer identyfikatorSv, Integer identyfikatorObcy, String loginPda, String nrTelefonu) {
        this.idBazodanowe = idBazodanowe;
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.plec = getPlec(plec);
        this.aktywny = aktywny;
        this.kierowca = kierowca;
        this.uzytkownikSystemuSV = uzytkownikSystemuSV;
        this.identyfikatorSv = identyfikatorSv;
        this.identyfikatorObcy = identyfikatorObcy;
        this.loginPda = loginPda;
        this.nrTelefonu = nrTelefonu;
    }

    private char getPlec(String plecString) {

        char result = 0;

        if (plecString != null && 1 == plecString.length()) {

            if (plecString.equals("M")) {

                result = 'M';

            } else if (plecString.equals("W")) {

                result = 'K';
            }
        }

        return result;
    }

    public Long getIdBazodanowe() {
        return idBazodanowe;
    }

    public void setIdBazodanowe(Long idBazodanowe) {
        this.idBazodanowe = idBazodanowe;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public char getPlec() {
        return plec;
    }

    public void setPlec(char plec) {
        this.plec = plec;
    }

    public Boolean getAktywny() {
        return aktywny;
    }

    public void setAktywny(Boolean aktywny) {
        this.aktywny = aktywny;
    }

    public Boolean getKierowca() {
        return kierowca;
    }

    public void setKierowca(Boolean kierowca) {
        this.kierowca = kierowca;
    }

    public Boolean getUzytkownikSystemuSV() {
        return uzytkownikSystemuSV;
    }

    public void setUzytkownikSystemuSV(Boolean uzytkownikSystemuSV) {
        this.uzytkownikSystemuSV = uzytkownikSystemuSV;
    }

    public Integer getIdentyfikatorSv() {
        return identyfikatorSv;
    }

    public void setIdentyfikatorSv(Integer identyfikatorSv) {
        this.identyfikatorSv = identyfikatorSv;
    }

    public Integer getIdentyfikatorObcy() {
        return identyfikatorObcy;
    }

    public void setIdentyfikatorObcy(Integer identyfikatorObcy) {
        this.identyfikatorObcy = identyfikatorObcy;
    }

    public String getLoginPda() {
        return loginPda;
    }

    public void setLoginPda(String loginPda) {
        this.loginPda = loginPda;
    }

    public String getNrTelefonu() {
        return nrTelefonu;
    }

    public void setNrTelefonu(String nrTelefonu) {
        this.nrTelefonu = nrTelefonu;
    }
}
