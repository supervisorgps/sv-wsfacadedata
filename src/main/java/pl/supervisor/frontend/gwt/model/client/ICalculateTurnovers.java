package pl.supervisor.frontend.gwt.model.client;

import java.util.Date;

public interface ICalculateTurnovers {

	Date getStartTime();
	
	Date getEndTime();
	
	int getTurnoversInScope1();

	int getTurnoversInScope2();

	int getTurnoversInScope3();
	
	void setTurnoversInScope0AsPercent(int percentage);
	
	void setTurnoversInScope1AsPercent(int percentage);
	
	void setTurnoversInScope2AsPercent(int percentage);
	
	void setTurnoversInScope3AsPercent(int percentage);

}
