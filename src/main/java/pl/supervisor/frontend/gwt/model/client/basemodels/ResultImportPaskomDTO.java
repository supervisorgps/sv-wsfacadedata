package pl.supervisor.frontend.gwt.model.client.basemodels;

import org.codehaus.jackson.annotate.JsonIgnore;

import java.io.Serializable;
import java.util.List;

/**
 * Created by abarczewski on 2015-01-14.
 */
public class ResultImportPaskomDTO implements Serializable {

    private String idImport;

    private List<String> resultMessagesCode;

    @JsonIgnore
    private String errorCode;

    public ResultImportPaskomDTO() {
    }

    public ResultImportPaskomDTO(String idImport, List<String> resultMessagesCode) {
        this.idImport = idImport;
        this.resultMessagesCode = resultMessagesCode;
    }

    public ResultImportPaskomDTO(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getIdImport() {
        return idImport;
    }

    public void setIdImport(String idImport) {
        this.idImport = idImport;
    }

    public List<String> getResultMessagesCode() {
        return resultMessagesCode;
    }

    public void setResultMessagesCode(List<String> resultMessagesCode) {
        this.resultMessagesCode = resultMessagesCode;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    @Override
    public String toString() {
        return "ResultImportPaskomDTO{" +
                "idImport='" + idImport + '\'' +
                ", resultMessagesCode=" + resultMessagesCode +
                '}';
    }
}
