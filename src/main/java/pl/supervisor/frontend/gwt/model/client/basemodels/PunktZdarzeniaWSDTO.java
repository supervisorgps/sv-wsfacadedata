package pl.supervisor.frontend.gwt.model.client.basemodels;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by norbertl on 19.06.16.
 */
public class PunktZdarzeniaWSDTO implements Serializable{

    private Long id;

    private Long zdarzenieId;

    private Integer typ;
    private Double lat;
    private Double lon;
    private Date time;

    private String kraj;
    private String wojewodztwo;
    private String powiat;
    private String gmina;
    private String droga;
    private String miejscowosc;
    private String kodPocztowy;
    private String ulica;
    private String posesja;

    private Boolean zdegeokodowanoAdres;

    public PunktZdarzeniaWSDTO() {
    }

    public PunktZdarzeniaWSDTO(Long id, Long zdarzenieId, Integer typ, Double lat, Double lon, Date time, String kraj, String wojewodztwo, String powiat, String gmina, String droga, String miejscowosc, String kodPocztowy, String ulica, String posesja, Boolean zdegeokodowanoAdres) {
        this.id = id;
        this.zdarzenieId = zdarzenieId;
        this.typ = typ;
        this.lat = lat;
        this.lon = lon;
        this.time = time;
        this.kraj = kraj;
        this.wojewodztwo = wojewodztwo;
        this.powiat = powiat;
        this.gmina = gmina;
        this.droga = droga;
        this.miejscowosc = miejscowosc;
        this.kodPocztowy = kodPocztowy;
        this.ulica = ulica;
        this.posesja = posesja;
        this.zdegeokodowanoAdres = zdegeokodowanoAdres;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getZdarzenieId() {
        return zdarzenieId;
    }

    public void setZdarzenieId(Long zdarzenieId) {
        this.zdarzenieId = zdarzenieId;
    }

    public Integer getTyp() {
        return typ;
    }

    public void setTyp(Integer typ) {
        this.typ = typ;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getKraj() {
        return kraj;
    }

    public void setKraj(String kraj) {
        this.kraj = kraj;
    }

    public String getWojewodztwo() {
        return wojewodztwo;
    }

    public void setWojewodztwo(String wojewodztwo) {
        this.wojewodztwo = wojewodztwo;
    }

    public String getPowiat() {
        return powiat;
    }

    public void setPowiat(String powiat) {
        this.powiat = powiat;
    }

    public String getGmina() {
        return gmina;
    }

    public void setGmina(String gmina) {
        this.gmina = gmina;
    }

    public String getDroga() {
        return droga;
    }

    public void setDroga(String droga) {
        this.droga = droga;
    }

    public String getMiejscowosc() {
        return miejscowosc;
    }

    public void setMiejscowosc(String miejscowosc) {
        this.miejscowosc = miejscowosc;
    }

    public String getKodPocztowy() {
        return kodPocztowy;
    }

    public void setKodPocztowy(String kodPocztowy) {
        this.kodPocztowy = kodPocztowy;
    }

    public String getUlica() {
        return ulica;
    }

    public void setUlica(String ulica) {
        this.ulica = ulica;
    }

    public String getPosesja() {
        return posesja;
    }

    public void setPosesja(String posesja) {
        this.posesja = posesja;
    }

    public Boolean getZdegeokodowanoAdres() {
        return zdegeokodowanoAdres;
    }

    public void setZdegeokodowanoAdres(Boolean zdegeokodowanoAdres) {
        this.zdegeokodowanoAdres = zdegeokodowanoAdres;
    }
}
