package pl.supervisor.frontend.gwt.model.client.basemodels;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by norbertl on 09.09.16.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class SendSmsTO implements Serializable{

    @XmlElement(required = true)
    private String number;

    @XmlElement(required = true)
    private String recipientName;

    @XmlElement(required = true)
    private Date smsDatetime;

    @XmlElement(required = true)
    private String contents;

    @XmlElement(required = true)
    private int category;

    @XmlElement(required = false)
    private String additionalData1;

    @XmlElement(required = false)
    private String additionalData2;

    @XmlElement(required = false)
    private String sender;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getRecipientName() {
        return recipientName;
    }

    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    public Date getSmsDatetime() {
        return smsDatetime;
    }

    public void setSmsDatetime(Date smsDatetime) {
        this.smsDatetime = smsDatetime;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public String getAdditionalData1() {
        return additionalData1;
    }

    public void setAdditionalData1(String additionalData1) {
        this.additionalData1 = additionalData1;
    }

    public String getAdditionalData2() {
        return additionalData2;
    }

    public void setAdditionalData2(String additionalData2) {
        this.additionalData2 = additionalData2;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("SendSmsTO{");
        sb.append("number='").append(number).append('\'');
        sb.append(", recipientName='").append(recipientName).append('\'');
        sb.append(", smsDatetime=").append(smsDatetime);
        sb.append(", contents='").append(contents).append('\'');
        sb.append(", category=").append(category);
        sb.append(", additionalData1='").append(additionalData1).append('\'');
        sb.append(", additionalData2='").append(additionalData2).append('\'');
        sb.append(", sender='").append(sender).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
