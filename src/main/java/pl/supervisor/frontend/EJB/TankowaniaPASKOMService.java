package pl.supervisor.frontend.EJB;

import pl.supervisor.frontend.gwt.model.client.basemodels.ResultImportPaskomDTO;
import pl.supervisor.frontend.gwt.model.client.basemodels.TankowaniePaskomDTO;

import java.util.List;

/**
 * Created by abarczewski on 2015-01-13.
 */
public interface TankowaniaPASKOMService {
    List<ResultImportPaskomDTO> importInvoices(String login, String password, List<TankowaniePaskomDTO> tankowaniePaskomDTOList);
}
