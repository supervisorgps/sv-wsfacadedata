package pl.supervisor.frontend.EJB;

import java.util.Date;
import java.util.List;

import pl.supervisor.frontend.gwt.model.client.basemodels.WizytaDaneWSDTO;

public interface IPlanowanaWizyta {

	public abstract List<WizytaDaneWSDTO> pobierzWizyty(String login,
			String haslo, Date dataCzasPierwszejWizyty,
			Date dataCzasOstatniejWizyty) throws Exception;

}