package pl.supervisor.frontend.EJB;

import java.util.Date;
import java.util.List;

import pl.supervisor.frontend.gwt.model.client.basemodels.PozycjeWSDTO;
import pl.supervisor.frontend.gwt.model.client.exception.BackendException;

public interface IPozycje {

	public abstract List<PozycjeWSDTO> getPositionsWithGeoPointAndRegistrationNumber(String login, String haslo, Date odDaty, Date doDaty, boolean onlyDegeocoded) throws BackendException;

}