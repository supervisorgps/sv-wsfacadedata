package pl.supervisor.frontend.EJB.validation;

import java.io.Serializable;

/**
 * Created by norbertl on 23.09.16.
 */
public class SmsSendingResult implements Serializable,Comparable<SmsSendingResult>{

    private String smsSendingId;
    private String sendingStatus;

    public SmsSendingResult() {
    }

    public SmsSendingResult(String smsSendingId, String sendingStatus) {
        this.smsSendingId = smsSendingId;
        this.sendingStatus = sendingStatus;
    }

    public String getSmsSendingId() {
        return smsSendingId;
    }

    public void setSmsSendingId(String smsSendingId) {
        this.smsSendingId = smsSendingId;
    }

    public String getSendingStatus() {
        return sendingStatus;
    }

    public void setSendingStatus(String sendingStatus) {
        this.sendingStatus = sendingStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SmsSendingResult that = (SmsSendingResult) o;

        return smsSendingId.equals(that.smsSendingId);

    }

    @Override
    public int hashCode() {
        return smsSendingId.hashCode();
    }

    @Override
    public int compareTo(SmsSendingResult o) {
        return getSmsSendingId().compareTo(o.getSmsSendingId());
    }
}

