package pl.supervisor.frontend.EJB.validation;

import java.io.Serializable;

/**
 * Created by norbertl on 09.02.16.
 */
public class WsResult implements Serializable {

    private boolean result = true;
    private String details = "OK";

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }
}
