package pl.supervisor.frontend.EJB;

import pl.supervisor.frontend.gwt.model.client.exception.BackendException;
import pl.supervisor.webservices.model.ZdarzeniePelneWSDTO;
import pl.supervisor.webservices.model.ZdarzenieTemperaturyWSDTO;
import pl.supervisor.webservices.model.ZdarzenieWSDTO;

import java.util.Date;
import java.util.List;

public interface IOdczytZdarzenie {

	// obsĹuguje zapytanie o obroty (PBDI)
	public abstract List<ZdarzeniePelneWSDTO> pobierzPelneZdarzeniaOdczytJazdaPostojUrz1Urz2(
			String login, String haslo, Date odDataCzas, Date doDataCzas)
			throws BackendException;

	public abstract List<ZdarzeniePelneWSDTO> pobierzPelneZdarzeniaOdczytJazdaPostojUrz1(
			String login, String haslo, Long lastId) throws BackendException;

	public abstract List<ZdarzeniePelneWSDTO> pobierzPelneZdarzeniaOdczytJazdaPostoj(
			String login, String haslo, Long lastId) throws BackendException;

	public abstract List<ZdarzenieWSDTO> pobierzZdarzenia(String login,
														  String haslo, Date odDaty, Date doDaty) throws BackendException;

	public List<ZdarzenieTemperaturyWSDTO> pobierzZdarzeniaPrzekroczeniaTemperaturyPojazdow(String login, String haslo, Date odDataCzas, Date doDataCzas, Double dolnaGranica, Double gornaGranica, Integer numerTermometru) throws BackendException;

}