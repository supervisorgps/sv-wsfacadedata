package pl.supervisor.frontend.EJB;

import pl.supervisor.frontend.gwt.model.client.basemodels.PunktZdarzeniaWSDTO;

import java.util.List;

/**
 * Created by norbertl on 19.06.16.
 */
public interface IPunktZdarzenia {

    public List<PunktZdarzeniaWSDTO> pobierzPunktyZdarzen(String login, String password, List<Long> idZdarzen);
}
