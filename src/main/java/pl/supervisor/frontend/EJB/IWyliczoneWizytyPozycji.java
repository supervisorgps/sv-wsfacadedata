package pl.supervisor.frontend.EJB;

import pl.supervisor.frontend.gwt.model.client.basemodels.WyliczonaWizytaPozycjiXLSDTO;
import pl.supervisor.frontend.gwt.model.client.exception.BackendException;
import pl.supervisor.webservices.model.ZapytanieRelizacji;

import java.util.Date;
import java.util.List;

public interface IWyliczoneWizytyPozycji {

	public List<WyliczonaWizytaPozycjiXLSDTO> pobierzWyliczoneWizytyPozycji(String login, String haslo, Date odDaty, Date doDaty) throws BackendException;

	public List<WyliczonaWizytaPozycjiXLSDTO> pobierzWyliczoneWizytyPda(String login, String haslo, ZapytanieRelizacji zlecenie) throws BackendException;

}