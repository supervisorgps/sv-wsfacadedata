package pl.supervisor.frontend.EJB;

import pl.supervisor.webservices.model.*;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by norbertl on 12.06.16.
 */
public interface IZleceniaPDA {

    public void zapiszRealizacjePda(PdaOrderRealization2 realizacjaPda);

    public List<PdaPosition> getPdaVehiclesPositions(String login, String password, Date from, Date to);

    public List<PdaPosition> getPdaOrders(String login, String password, Date from, Date to);

    public Map<Integer, List<MapPointTO>> getTrackForOrder(String login, String password, ZapytanieRelizacji orderRequest, int trackType);

    public PdaPosition getPdaOrder(String login, String password, ZapytanieRelizacji orderRequest);

    public List<PunktWSTO> calculateDistancesAndTimes(List positions);

    public List<PunktWSTO> getMapTrackPoints( String login, String password, ZapytanieRelizacji orderRequest);

    public List<PdaPosition> getOrdersByNumberAndDriverTuples(String login, String password, List<PdaOrderRequest> ordersRequest);

    public String[] getVehicleAndDriverPhoneNumber(String login, String password, String driverLogin, Date from);
}
