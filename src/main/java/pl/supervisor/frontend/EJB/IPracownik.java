package pl.supervisor.frontend.EJB;

import pl.supervisor.frontend.gwt.model.client.basemodels.PracownikLoginRequest;
import pl.supervisor.frontend.gwt.model.client.basemodels.PracownikLoginResponse;
import pl.supervisor.frontend.gwt.model.client.basemodels.PracownikWSDTO;

import java.util.List;

/**
 * Created by norbertl on 18.03.16.
 */
public interface IPracownik {

    public abstract List<PracownikWSDTO> pobierzPraconwnikow(String login, String haslo,
                                                             Boolean uzytkownicySystemu,
                                                             Boolean tylkoAktywni,
                                                             Boolean kierowcy);


    PracownikLoginResponse login(PracownikLoginRequest loginRequest);
}
