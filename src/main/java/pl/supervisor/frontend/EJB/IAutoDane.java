package pl.supervisor.frontend.EJB;

import pl.supervisor.frontend.gwt.model.client.basemodels.AutoLocalisationDTO;
import pl.supervisor.frontend.gwt.model.client.exception.BackendException;
import pl.supervisor.webservices.model.AutoDaneDto;
import pl.supervisor.webservices.model.AutoDaneWSTO;

import java.util.List;
import java.util.Map;

public interface IAutoDane {


    List<AutoDaneWSTO> getAllAutoDaneByClientId(String login, String password) throws BackendException;

    List<AutoDaneWSTO> getAutoDaneByIds(String login, String password, Long idFrom, Long idTo) throws BackendException;

    List<AutoDaneDto> getAllCars(String login, String password) throws BackendException;

    List<AutoLocalisationDTO> getCarsWithPositions(String login, String password) throws BackendException;
}
