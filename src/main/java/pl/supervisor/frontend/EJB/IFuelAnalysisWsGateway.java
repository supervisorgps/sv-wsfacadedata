package pl.supervisor.frontend.EJB;

import pl.supervisor.frontend.gwt.model.client.basemodels.FuelAnalysisWSResult;

import java.util.Date;

/**
 * Created by norbertl on 25.10.16.
 */
public interface IFuelAnalysisWsGateway {

    FuelAnalysisWSResult fuealAnalysis(String login, String password, Date from, Date to, String vehicleRegNumber, Boolean newMode);
}
