package pl.supervisor.frontend.EJB;

import java.util.List;

import pl.supervisor.frontend.gwt.model.client.exception.BackendException;
import pl.supervisor.webservices.model.KlientDto;

public interface IKlient {

	List<KlientDto> getAllKlients(String user, String password) throws BackendException;
	void setPlatnik(String user, String password,Integer klnId,boolean isPlatnik) throws BackendException;
}
