package pl.supervisor.frontend.EJB;

import pl.supervisor.frontend.EJB.validation.WsResult;
import pl.supervisor.frontend.gwt.model.client.basemodels.PunktWlasnyWSDTO;
import pl.supervisor.frontend.gwt.model.client.exception.BackendException;

import javax.xml.ws.Holder;
import java.util.List;

/**
 * Created by norbertl on 21.11.15.
 */
public interface IPunktWlasny {

    List<PunktWlasnyWSDTO> getPointsBySymbols(String login, String password, String[] symbols) throws BackendException;

    List<PunktWlasnyWSDTO> getPointsByNames(String login, String password, String[] names) throws BackendException;

    WsResult synchronizePoints(String login, String password, final Holder<List<PunktWlasnyWSDTO>> points);

    List<String> degeocodeLocation( String location);

    WsResult savePoint(String login, String password, final Holder<PunktWlasnyWSDTO> point);
}