package pl.supervisor.frontend.EJB;

import pl.supervisor.frontend.EJB.validation.SmsSendingResult;
import pl.supervisor.frontend.gwt.model.client.basemodels.SendSmsTO;
import pl.supervisor.frontend.gwt.model.client.basemodels.SmsNotificationTO;

import java.util.List;

/**
 * Created by norbertl on 21.11.15.
 */
public interface ISMSGateway {

    SmsSendingResult sendSms(String login, String password, SendSmsTO smsObject);

    List<SmsSendingResult> checkSmsStatuses(String login, String password, List<String> smsSendingIds);

    List<SmsSendingResult> checkSmsStatusesRightNow(String login, String password, List<String> smsSendingIds);

    void saveSmsNotification(String login, String password, SmsNotificationTO model);
}
