package pl.supervisor.frontend.EJB;

import java.util.Date;
import java.util.List;

import pl.supervisor.frontend.gwt.model.client.basemodels.WyliczonaWizytaXLSDTO;
import pl.supervisor.frontend.gwt.model.client.exception.BackendException;

public interface IWyliczoneWizyty {
	
	public List<WyliczonaWizytaXLSDTO> pobierzWyliczoneWizyty(String login, String haslo, Date odDaty, Date doDaty) throws BackendException;
	
}