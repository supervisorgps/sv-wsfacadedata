package pl.supervisor.frontend.EJB;

import pl.supervisor.frontend.administration.Ladunek;
import pl.supervisor.frontend.gwt.model.client.exception.BackendException;

import java.util.List;

/**
 * Created by norbertl on 12.08.16.
 */
public interface ILadunek {

    List<Ladunek> pobierzLadunki(String login, String password, String rodzaj) throws BackendException;
}
