package pl.supervisor.frontend.EJB;

import pl.supervisor.frontend.gwt.model.client.exception.BackendException;
import pl.supervisor.webservices.model.PunktWSTO;
import pl.supervisor.webservices.model.TrasaWizytWSTO;

import javax.jws.WebParam;
import java.util.List;

/**
 * Created by norbertl on 17.11.15.
 */
public interface ITrasaWizytBean {

	TrasaWizytWSTO wyslijTrase(@WebParam(name = "login") String login, @WebParam(name = "haslo") String haslo, @WebParam(name = "trasa") TrasaWizytWSTO trasa, RouteCalculationMode mode) throws BackendException;

	TrasaWizytWSTO wyslijTraseJspirit(@WebParam(name = "login") String login, @WebParam(name = "haslo") String haslo, @WebParam(name = "trasa") TrasaWizytWSTO trasa, RouteCalculationMode mode) throws BackendException;

	List<PunktWSTO> przeliczDystanseCzasy(String login, String haslo, List<PunktWSTO> punkty, RouteCalculationMode mode);

	String wygenerujGpx(String login, String haslo, List<PunktWSTO> punkty, RouteCalculationMode tryb);

	enum RouteCalculationMode{
		FASTEST, SHORTEST;
	}
}
