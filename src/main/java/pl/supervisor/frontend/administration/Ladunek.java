package pl.supervisor.frontend.administration;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="ladunek")
@SequenceGenerator(name="LAD_SEQ", sequenceName="ladunek_lad_id_seq")
public class Ladunek implements Serializable{

	private Long id;
	private Integer klnId;
	private String rodzaj;
	private String numer;

	private String nazwa1;
	private String nazwa2;

	private String opis1;
	private String opis2;
	private String opis3;

	private String idSprzetu1;
	private String idSprzetu2;

	@Id
	@Column(name="lad_id")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="LAD_SEQ")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "lad_kln_id")
	public Integer getKlnId() {
		return klnId;
	}

	public void setKlnId(Integer klnId) {
		this.klnId = klnId;
	}

	@Column(name = "lad_rodzaj")
	public String getRodzaj() {
		return rodzaj;
	}

	public void setRodzaj(String rodzaj) {
		this.rodzaj = rodzaj;
	}

	@Column(name = "lad_numer")
	public String getNumer() {
		return numer;
	}

	public void setNumer(String numer) {
		this.numer = numer;
	}

	@Column(name = "lad_nazwa_1")
	public String getNazwa1() {
		return nazwa1;
	}

	public void setNazwa1(String nazwa1) {
		this.nazwa1 = nazwa1;
	}

	@Column(name = "lad_nazwa_2")
	public String getNazwa2() {
		return nazwa2;
	}

	public void setNazwa2(String nazwa2) {
		this.nazwa2 = nazwa2;
	}

	@Column(name = "lad_opis_1")
	public String getOpis1() {
		return opis1;
	}

	public void setOpis1(String opis1) {
		this.opis1 = opis1;
	}

	@Column(name = "lad_opis_2")
	public String getOpis2() {
		return opis2;
	}

	public void setOpis2(String opis2) {
		this.opis2 = opis2;
	}

	@Column(name = "lad_opis_3")
	public String getOpis3() {
		return opis3;
	}

	public void setOpis3(String opis3) {
		this.opis3 = opis3;
	}

	@Column(name = "lad_id_sprzetu_1")
	public String getIdSprzetu1() {
		return idSprzetu1;
	}

	public void setIdSprzetu1(String idSprzetu1) {
		this.idSprzetu1 = idSprzetu1;
	}

	@Column(name = "lad_id_sprzetu_2")
	public String getIdSprzetu2() {
		return idSprzetu2;
	}

	public void setIdSprzetu2(String idSprzetu2) {
		this.idSprzetu2 = idSprzetu2;
	}
}
