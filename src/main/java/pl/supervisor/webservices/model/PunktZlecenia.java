package pl.supervisor.webservices.model;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="PunktZlecenia")
@XmlType(name="PunktZlecenia")
public class PunktZlecenia implements Serializable{

    @XmlElement(nillable = true, required = false)
    private Long idPunktu;
    private String symbolPunktu;
    @XmlElement(nillable = false, required = true)
    private String adresPunktuOpis;
    @XmlElement(nillable = true, required = false)
    private Date dataCzasWizytyOd;
    @XmlElement(nillable = true, required = false)
    private Date dataCzasWizytyDo;
    @XmlElement(nillable = false, required = true)
    private List<Pasazer> pasazerowie = new ArrayList<Pasazer>(0);

    public Long getIdPunktu() {
        return idPunktu;
    }

    public void setIdPunktu(Long idPunktu) {
        this.idPunktu = idPunktu;
    }

    public String getSymbolPunktu() {
        return symbolPunktu;
    }

    public void setSymbolPunktu(String symbolPunktu) {
        this.symbolPunktu = symbolPunktu;
    }

    public String getAdresPunktuOpis() {
        return adresPunktuOpis;
    }

    public void setAdresPunktuOpis(String adresPunktuOpis) {
        this.adresPunktuOpis = adresPunktuOpis;
    }

    public Date getDataCzasWizytyOd() {
        return dataCzasWizytyOd;
    }

    public void setDataCzasWizytyOd(Date dataCzasWizytyOd) {
        this.dataCzasWizytyOd = dataCzasWizytyOd;
    }

    public Date getDataCzasWizytyDo() {
        return dataCzasWizytyDo;
    }

    public void setDataCzasWizytyDo(Date dataCzasWizytyDo) {
        this.dataCzasWizytyDo = dataCzasWizytyDo;
    }

    public List<Pasazer> getPasazerowie() {
        return pasazerowie;
    }

    public void setPasazerowie(List<Pasazer> pasazerowie) {
        this.pasazerowie = pasazerowie;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PunktZlecenia{");
        sb.append("idPunktu=").append(idPunktu);
        sb.append(", symbolPunktu='").append(symbolPunktu).append('\'');
        sb.append(", adresPunktuOpis='").append(adresPunktuOpis).append('\'');
        sb.append(", dataCzasWizytyOd=").append(dataCzasWizytyOd);
        sb.append(", dataCzasWizytyDo=").append(dataCzasWizytyDo);
        sb.append(", cargos=").append(Arrays.toString( pasazerowie.toArray()));
        sb.append('}');
        return sb.toString();
    }
}