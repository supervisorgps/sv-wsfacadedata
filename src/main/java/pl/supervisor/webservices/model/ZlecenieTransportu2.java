package pl.supervisor.webservices.model;

import org.tempuri.PunktZlecenia2;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


public class ZlecenieTransportu2 implements Serializable {

    private Long idZlecenia;
    private Date data;
    private String symbolZlecenia;
    private Integer idKierowcy;
    private String opisTekstowy;
    private List<PunktZlecenia2> punktyZlecenia = new ArrayList<PunktZlecenia2>(0);

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String getSymbolZlecenia() {
        return symbolZlecenia;
    }

    public void setSymbolZlecenia(String symbolZlecenia) {
        this.symbolZlecenia = symbolZlecenia;
    }

    public Integer getIdKierowcy() {
        return idKierowcy;
    }

    public void setIdKierowcy(Integer idKierowcy) {
        this.idKierowcy = idKierowcy;
    }

    public String getOpisTekstowy() {
        return opisTekstowy;
    }

    public void setOpisTekstowy(String opisTekstowy) {
        this.opisTekstowy = opisTekstowy;
    }

    public List<PunktZlecenia2> getPunktyZlecenia() {
        return punktyZlecenia;
    }

    public void setPunktyZlecenia(List<PunktZlecenia2> punktyZlecenia) {
        this.punktyZlecenia = punktyZlecenia;
    }

    public Long getIdZlecenia() {
        return idZlecenia;
    }

    public void setIdZlecenia(Long idZlecenia) {
        this.idZlecenia = idZlecenia;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ZlecenieTransportu{");
        sb.append("data=").append(data);
        sb.append(", idZlecenia='").append(idZlecenia).append('\'');
        sb.append(", symbolZlecenia='").append(symbolZlecenia).append('\'');
        sb.append(", idKierowcy=").append(idKierowcy);
        sb.append(", opisTekstowy='").append(opisTekstowy).append('\'');
        sb.append(", visits=").append(Arrays.toString( punktyZlecenia.toArray()));
        sb.append('}');
        return sb.toString();
    }
}
