package pl.supervisor.webservices.model;

import pl.supervisor.frontend.gwt.model.client.basemodels.WizytaWSTO;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.datatype.Duration;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@SuppressWarnings("serial")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "route", namespace = "sv.integration.ws.route")
public class TrasaWizytWSTO implements Serializable {

	@XmlElement(required = false)
	private Long id; // nadawane przez SV po zapisie

	@XmlElement(required = true)
	private String externalId; // w tym wypadku (CDG) guid wyjazdu

	@XmlElement(required = true)
	private Date plannedStartDateTime; // początek trasy - wyjazd z bazy

	@XmlElement(required = true)
	private Date plannedEndDateTime; // koniec trasy - powrót do bazy

	@XmlElement(required = true)
	private String vehicleRegistrationNumber; // numer rejestracyjny

	@XmlElement(required = false)
	private Integer routeMileage; // długość trasy w kilometrach

	@XmlElement(required = false)
	private Duration routeDuration; // czas trwania trasy

	@XmlElement(required = true)
	private RouteCommunicationMode communicationMode; // tryb komunikacji - określa kierunek oraz zlecenie / wynik przetwarzania

	@XmlElement(required = true, nillable = false)
	private List<WizytaWSTO> visits = new ArrayList<WizytaWSTO>(); // lista wizyt składająca się na trasę

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public Date getPlannedStartDateTime() {
		return plannedStartDateTime;
	}

	public void setPlannedStartDateTime(Date plannedStartDateTime) {
		this.plannedStartDateTime = plannedStartDateTime;
	}

	public Date getPlannedEndDateTime() {
		return plannedEndDateTime;
	}

	public void setPlannedEndDateTime(Date plannedEndDateTime) {
		this.plannedEndDateTime = plannedEndDateTime;
	}

	public String getVehicleRegistrationNumber() {
		return vehicleRegistrationNumber;
	}

	public void setVehicleRegistrationNumber(String vehicleRegistrationNumber) {
		this.vehicleRegistrationNumber = vehicleRegistrationNumber;
	}

	public Integer getRouteMileage() {
		return routeMileage;
	}

	public void setRouteMileage(Integer routeMileage) {
		this.routeMileage = routeMileage;
	}

	public Duration getRouteDuration() {
		return routeDuration;
	}

	public void setRouteDuration(Duration routeDuration) {
		this.routeDuration = routeDuration;
	}

	public RouteCommunicationMode getCommunicationMode() {
		return communicationMode;
	}

	public void setCommunicationMode(RouteCommunicationMode communicationMode) {
		this.communicationMode = communicationMode;
	}

	public List<WizytaWSTO> getVisits() {
		return visits;
	}

	public void setVisits(List<WizytaWSTO> visits) {
		this.visits = visits;
	}

	/**
	 * Prawdopodobnie nie będzie używany nigdzie indziej
	 * @author norbertl
	 *
	 */
	public enum RouteCommunicationMode {
		TO_RECALCULATE_QUICK, TO_RECALCULATE_SHORT, TO_SAVE, TO_UPDATE, RECALCULATED, SAVED, UPDATED, CALCULATION_ERROR, SAVE_ERROR, UPDATE_ERROR
	}

}
