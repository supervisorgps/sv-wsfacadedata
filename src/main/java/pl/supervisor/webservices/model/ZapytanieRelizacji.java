package pl.supervisor.webservices.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;

/**
 * Created by norbertl on 11.05.16.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class ZapytanieRelizacji implements Serializable {

    @XmlElement(name = "idZlecenia", required = true, type = Integer.class, nillable = true)
    private Integer idZlecenia;

    @XmlElement(name = "loginKierowcy", required = true, type = String.class, nillable = true)
    private String loginKierowcy;

    public boolean hasValidParams() {

        return idZlecenia != null && loginKierowcy != null;
    }

    public Integer getIdZlecenia() {
        return idZlecenia;
    }

    public void setIdZlecenia(Integer idZlecenia) {
        this.idZlecenia = idZlecenia;
    }

    public String getLoginKierowcy() {
        return loginKierowcy;
    }

    public void setLoginKierowcy(String loginKierowcy) {
        this.loginKierowcy = loginKierowcy;
    }
}
