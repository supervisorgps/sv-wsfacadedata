package pl.supervisor.webservices.model;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.tempuri.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="ZlecenieTransportu")
@XmlType(name="ZlecenieTransportu")
public class ZlecenieTransportu implements Serializable {

    @XmlElement(nillable = false, required = true)
    private Long idZlecenia;
    @XmlElement(nillable = false, required = true)
    private Date data;
    @XmlElement(nillable = false, required = true)
    private String symbolZlecenia;
    @XmlElement(nillable = false, required = true)
    private String loginKierowcy;
    @XmlElement(nillable = false, required = true)
    private String opisTekstowy;
    @XmlElement(nillable = false, required = true)
    private List<PunktZlecenia> punktyZlecenia = new ArrayList<PunktZlecenia>(0);

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String getSymbolZlecenia() {
        return symbolZlecenia;
    }

    public void setSymbolZlecenia(String symbolZlecenia) {
        this.symbolZlecenia = symbolZlecenia;
    }

    public String getLoginKierowcy() {
        return loginKierowcy;
    }

    public void setLoginKierowcy(String loginKierowcy) {
        this.loginKierowcy = loginKierowcy;
    }

    public String getOpisTekstowy() {
        return opisTekstowy;
    }

    public void setOpisTekstowy(String opisTekstowy) {
        this.opisTekstowy = opisTekstowy;
    }

    public List<PunktZlecenia> getPunktyZlecenia() {
        return punktyZlecenia;
    }

    public void setPunktyZlecenia(List<PunktZlecenia> punktyZlecenia) {
        this.punktyZlecenia = punktyZlecenia;
    }

    public Long getIdZlecenia() {
        return idZlecenia;
    }

    public void setIdZlecenia(Long idZlecenia) {
        this.idZlecenia = idZlecenia;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ZlecenieTransportu{");
        sb.append("data=").append(data);
        sb.append(", idZlecenia='").append(idZlecenia).append('\'');
        sb.append(", symbolZlecenia='").append(symbolZlecenia).append('\'');
        sb.append(", loginKierowcy=").append(loginKierowcy);
        sb.append(", opisTekstowy='").append(opisTekstowy).append('\'');
        sb.append(", visits=").append(Arrays.toString( punktyZlecenia.toArray()));
        sb.append('}');
        return sb.toString();
    }
}
