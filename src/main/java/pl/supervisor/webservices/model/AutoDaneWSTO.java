package pl.supervisor.webservices.model;

public class AutoDaneWSTO implements java.io.Serializable{

	private String regNumber;
	private String symbol;
	private String make;
	private String model;
	private String type;
	private Integer fuelTank1Capacity;
	private Integer fuelTank2Capacity;
	private Double engineNormativeDuringDrive;
	private Double engineNormativeDuringStop;
	
	// obsługa obrotomierza - PBDI
	private Boolean tachometer;
	private Long turnoverLimit1;
	private Long turnoverLimit2;
	private Long turnoverLimit3;
	private Short turnoverRangesNumber;
	private String range1Name;
	private String range2Name;
	private String range3Name;
	private String range4Name;
	private String device1Name;
	private String device2Name;
	private String digitalDevice1Name;
	private String digitalDevice2Name;
	private String termometer1Name;
	private String termometer2Name;
	private String termometer3Name;
	private Boolean assaultButton;

	public AutoDaneWSTO(String regNumber, String symbol, String make, String model, String type, Integer fuelTank1Capacity, Integer fuelTank2Capacity,
			Double engineNormativeDuringDrive, Double engineNormativeDuringStop, Boolean tachometer, Long turnoverLimit1, Long turnoverLimit2,
			Long turnoverLimit3, Short turnoverRangesNumber, String range1Name, String range2Name, String range3Name, String range4Name,
			String device1Name, String device2Name, String digitalDevice1Name, String digitalDevice2Name,
			String termometer1Name, String termometer2Name, String termometer3Name, Boolean assaultButton) {

		this.regNumber = regNumber;
		this.symbol = symbol;
		this.make = make;
		this.model = model;
		this.type = type;
		this.fuelTank1Capacity = fuelTank1Capacity;
		this.fuelTank2Capacity = fuelTank2Capacity;
		this.engineNormativeDuringDrive = engineNormativeDuringDrive;
		this.engineNormativeDuringStop = engineNormativeDuringStop;
		this.tachometer = tachometer;
		this.turnoverLimit1 = turnoverLimit1;
		this.turnoverLimit2 = turnoverLimit2;
		this.turnoverLimit3 = turnoverLimit3;
		this.turnoverRangesNumber = turnoverRangesNumber;
		this.range1Name = range1Name;
		this.range2Name = range2Name;
		this.range3Name = range3Name;
		this.range4Name = range4Name;
		this.device1Name = device1Name;
		this.device2Name = device2Name;
		this.digitalDevice1Name = digitalDevice1Name;
		this.digitalDevice2Name = digitalDevice2Name;
		this.termometer1Name = termometer1Name;
		this.termometer2Name = termometer2Name;
		this.termometer3Name = termometer3Name;
		this.assaultButton = assaultButton;
		
	}

	public String getRegNumber() {
		return regNumber;
	}

	public void setRegNumber(String regNumber) {
		this.regNumber = regNumber;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getMake() {
		return make;
	}

	public void setMake(String make) {
		this.make = make;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getFuelTank1Capacity() {
		return fuelTank1Capacity;
	}

	public void setFuelTank1Capacity(Integer fuelTank1Capacity) {
		this.fuelTank1Capacity = fuelTank1Capacity;
	}

	public Integer getFuelTank2Capacity() {
		return fuelTank2Capacity;
	}

	public void setFuelTank2Capacity(Integer fuelTank2Capacity) {
		this.fuelTank2Capacity = fuelTank2Capacity;
	}

	public Double getEngineNormativeDuringDrive() {
		return engineNormativeDuringDrive;
	}

	public void setEngineNormativeDuringDrive(Double engineNormativeDuringDrive) {
		this.engineNormativeDuringDrive = engineNormativeDuringDrive;
	}

	public Double getEngineNormativeDuringStop() {
		return engineNormativeDuringStop;
	}

	public void setEngineNormativeDuringStop(Double engineNormativeDuringStop) {
		this.engineNormativeDuringStop = engineNormativeDuringStop;
	}

	public Boolean getTachometer() {
		return tachometer;
	}

	public void setTachometer(Boolean tachometer) {
		this.tachometer = tachometer;
	}

	public Long getTurnoverLimit1() {
		return turnoverLimit1;
	}

	public void setTurnoverLimit1(Long turnoverLimit1) {
		this.turnoverLimit1 = turnoverLimit1;
	}

	public Long getTurnoverLimit2() {
		return turnoverLimit2;
	}

	public void setTurnoverLimit2(Long turnoverLimit2) {
		this.turnoverLimit2 = turnoverLimit2;
	}

	public Long getTurnoverLimit3() {
		return turnoverLimit3;
	}

	public void setTurnoverLimit3(Long turnoverLimit3) {
		this.turnoverLimit3 = turnoverLimit3;
	}

	public Short getTurnoverRangesNumber() {
		return turnoverRangesNumber;
	}

	public void setTurnoverRangesNumber(Short turnoverRangesNumber) {
		this.turnoverRangesNumber = turnoverRangesNumber;
	}

	public String getRange1Name() {
		return range1Name;
	}

	public void setRange1Name(String range1Name) {
		this.range1Name = range1Name;
	}

	public String getRange2Name() {
		return range2Name;
	}

	public void setRange2Name(String range2Name) {
		this.range2Name = range2Name;
	}

	public String getRange3Name() {
		return range3Name;
	}

	public void setRange3Name(String range3Name) {
		this.range3Name = range3Name;
	}

	public String getRange4Name() {
		return range4Name;
	}

	public void setRange4Name(String range4Name) {
		this.range4Name = range4Name;
	}

	public String getDevice1Name() {
		return device1Name;
	}

	public void setDevice1Name(String device1Name) {
		this.device1Name = device1Name;
	}

	public String getDevice2Name() {
		return device2Name;
	}

	public void setDevice2Name(String device2Name) {
		this.device2Name = device2Name;
	}

	public String getDigitalDevice1Name() {
		return digitalDevice1Name;
	}

	public void setDigitalDevice1Name(String digitalDevice1Name) {
		this.digitalDevice1Name = digitalDevice1Name;
	}

	public String getDigitalDevice2Name() {
		return digitalDevice2Name;
	}

	public void setDigitalDevice2Name(String digitalDevice2Name) {
		this.digitalDevice2Name = digitalDevice2Name;
	}

	public String getTermometer1Name() {
		return termometer1Name;
	}

	public void setTermometer1Name(String termometer1Name) {
		this.termometer1Name = termometer1Name;
	}

	public String getTermometer2Name() {
		return termometer2Name;
	}

	public void setTermometer2Name(String termometer2Name) {
		this.termometer2Name = termometer2Name;
	}

	public String getTermometer3Name() {
		return termometer3Name;
	}

	public void setTermometer3Name(String termometer3Name) {
		this.termometer3Name = termometer3Name;
	}

	public Boolean getAssaultButton() {
		return assaultButton;
	}

	public void setAssaultButton(Boolean assaultButton) {
		this.assaultButton = assaultButton;
	}
}
