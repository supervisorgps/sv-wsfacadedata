package pl.supervisor.webservices.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by norbertl on 10.05.16.
 */
public class PdaVisitCargoRealization implements Serializable {

    private String cargoId;
    private String cargoAdditionalInfo1;
    private String cargoAdditionalInfo2;

    private Double longitude;
    private Double latitude;

    private Integer locationPrecision;

    private String cargoOperation; // IN/OUT

    private Date operationDate;

    public String getCargoId() {
        return cargoId;
    }

    public void setCargoId(String cargoId) {
        this.cargoId = cargoId;
    }

    public String getCargoAdditionalInfo1() {
        return cargoAdditionalInfo1;
    }

    public void setCargoAdditionalInfo1(String cargoAdditionalInfo1) {
        this.cargoAdditionalInfo1 = cargoAdditionalInfo1;
    }

    public String getCargoAdditionalInfo2() {
        return cargoAdditionalInfo2;
    }

    public void setCargoAdditionalInfo2(String cargoAdditionalInfo2) {
        this.cargoAdditionalInfo2 = cargoAdditionalInfo2;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Integer getLocationPrecision() {
        return locationPrecision;
    }

    public void setLocationPrecision(Integer locationPrecision) {
        this.locationPrecision = locationPrecision;
    }

    public String getCargoOperation() {
        return cargoOperation;
    }

    public void setCargoOperation(String cargoOperation) {
        this.cargoOperation = cargoOperation;
    }

    public Date getOperationDate() {
        return operationDate;
    }

    public void setOperationDate(Date operationDate) {
        this.operationDate = operationDate;
    }
}
