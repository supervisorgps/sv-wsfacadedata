package pl.supervisor.webservices.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class KlientDto implements Serializable {
    /**
	 * 
	 */
	private Integer klnId;
    private String klnNazwaFirm;
    private String klnNip;
    
    private String klnWojewodztwo;
    private String klnMiasto;
    private String klnUlica;
    private String klnKod;
    private String klnImie;
    private String klnNazwisko;
    private String klnTelefonKontaktowy;
    private String klnTelefonFirmowy;
    
    private String klnDataZarejestrowania;
    private String klnDataWyrejestrowania;
    private String klnDataZawieszenia;
    private String klnDataOdwieszenia;
   
    private String emailFirmowy;
    private boolean klnSzczegolowaMapa;
   
    private boolean klnPartner;
    private Long klnKlnId;
    
    private boolean pokazWiadomoscLogowanie;
    private boolean pokazWiadomoscPraca;
   
    private boolean platnik;
    
    private String wiadomoscLogowanie;
   
    private String wiadomoscPraca;

	public Integer getKlnId() {
		return klnId;
	}

	public void setKlnId(Integer klnId) {
		this.klnId = klnId;
	}

	public String getKlnNazwaFirm() {
		return klnNazwaFirm;
	}

	public void setKlnNazwaFirm(String klnNazwaFirm) {
		this.klnNazwaFirm = klnNazwaFirm;
	}

	public String getKlnNip() {
		return klnNip;
	}

	public void setKlnNip(String klnNip) {
		this.klnNip = klnNip;
	}

	public String getKlnWojewodztwo() {
		return klnWojewodztwo;
	}

	public void setKlnWojewodztwo(String klnWojewodztwo) {
		this.klnWojewodztwo = klnWojewodztwo;
	}

	public String getKlnMiasto() {
		return klnMiasto;
	}

	public void setKlnMiasto(String klnMiasto) {
		this.klnMiasto = klnMiasto;
	}

	public String getKlnUlica() {
		return klnUlica;
	}

	public void setKlnUlica(String klnUlica) {
		this.klnUlica = klnUlica;
	}

	public String getKlnKod() {
		return klnKod;
	}

	public void setKlnKod(String klnKod) {
		this.klnKod = klnKod;
	}

	public String getKlnImie() {
		return klnImie;
	}

	public void setKlnImie(String klnImie) {
		this.klnImie = klnImie;
	}

	public String getKlnNazwisko() {
		return klnNazwisko;
	}

	public void setKlnNazwisko(String klnNazwisko) {
		this.klnNazwisko = klnNazwisko;
	}

	public String getKlnTelefonKontaktowy() {
		return klnTelefonKontaktowy;
	}

	public void setKlnTelefonKontaktowy(String klnTelefonKontaktowy) {
		this.klnTelefonKontaktowy = klnTelefonKontaktowy;
	}

	public String getKlnTelefonFirmowy() {
		return klnTelefonFirmowy;
	}

	public void setKlnTelefonFirmowy(String klnTelefonFirmowy) {
		this.klnTelefonFirmowy = klnTelefonFirmowy;
	}

	public String getKlnDataZarejestrowania() {
		return klnDataZarejestrowania;
	}

	public void setKlnDataZarejestrowania(String klnDataZarejestrowania) {
		this.klnDataZarejestrowania = klnDataZarejestrowania;
	}

	public String getKlnDataWyrejestrowania() {
		return klnDataWyrejestrowania;
	}

	public void setKlnDataWyrejestrowania(String klnDataWyrejestrowania) {
		this.klnDataWyrejestrowania = klnDataWyrejestrowania;
	}

	public String getKlnDataZawieszenia() {
		return klnDataZawieszenia;
	}

	public void setKlnDataZawieszenia(String klnDataZawieszenia) {
		this.klnDataZawieszenia = klnDataZawieszenia;
	}

	public String getKlnDataOdwieszenia() {
		return klnDataOdwieszenia;
	}

	public void setKlnDataOdwieszenia(String klnDataOdwieszenia) {
		this.klnDataOdwieszenia = klnDataOdwieszenia;
	}

	public String getEmailFirmowy() {
		return emailFirmowy;
	}

	public void setEmailFirmowy(String emailFirmowy) {
		this.emailFirmowy = emailFirmowy;
	}

	public boolean getKlnSzczegolowaMapa() {
		return klnSzczegolowaMapa;
	}

	public void setKlnSzczegolowaMapa(boolean klnSzczegolowaMapa) {
		this.klnSzczegolowaMapa = klnSzczegolowaMapa;
	}

	public boolean getKlnPartner() {
		return klnPartner;
	}

	public void setKlnPartner(boolean klnPartner) {
		this.klnPartner = klnPartner;
	}

	public Long getKlnKlnId() {
		return klnKlnId;
	}

	public void setKlnKlnId(Long klnKlnId) {
		this.klnKlnId = klnKlnId;
	}

	public boolean getPokazWiadomoscLogowanie() {
		return pokazWiadomoscLogowanie;
	}

	public void setPokazWiadomoscLogowanie(boolean pokazWiadomoscLogowanie) {
		this.pokazWiadomoscLogowanie = pokazWiadomoscLogowanie;
	}

	public boolean getPokazWiadomoscPraca() {
		return pokazWiadomoscPraca;
	}

	public void setPokazWiadomoscPraca(boolean pokazWiadomoscPraca) {
		this.pokazWiadomoscPraca = pokazWiadomoscPraca;
	}

	public boolean getPlatnik() {
		return platnik;
	}

	public void setPlatnik(boolean platnik) {
		this.platnik = platnik;
	}

	public String getWiadomoscLogowanie() {
		return wiadomoscLogowanie;
	}

	public void setWiadomoscLogowanie(String wiadomoscLogowanie) {
		this.wiadomoscLogowanie = wiadomoscLogowanie;
	}

	public String getWiadomoscPraca() {
		return wiadomoscPraca;
	}

	public void setWiadomoscPraca(String wiadomoscPraca) {
		this.wiadomoscPraca = wiadomoscPraca;
	}
}
