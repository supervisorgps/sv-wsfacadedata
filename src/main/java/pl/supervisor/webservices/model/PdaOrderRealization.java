package pl.supervisor.webservices.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by norbertl on 10.05.16.
 */
public class PdaOrderRealization implements Serializable{

    private static final long serialVersionUID = 1000L;

    private Integer orderId;
    private String orderSymbol;

    private String driverLogin;
    private Date realizationStart;
    private Date realizationEnd;

    private List<PdaVisitCargoRealization> items = new ArrayList<PdaVisitCargoRealization>(0);

    private List<PdaVisitLocationRealization> locations = new ArrayList<PdaVisitLocationRealization>();

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getOrderSymbol() {
        return orderSymbol;
    }

    public void setOrderSymbol(String orderSymbol) {
        this.orderSymbol = orderSymbol;
    }

    public String getDriverLogin() {
        return driverLogin;
    }

    public void setDriverLogin(String driverLogin) {
        this.driverLogin = driverLogin;
    }

    public Date getRealizationStart() {
        return realizationStart;
    }

    public void setRealizationStart(Date realizationStart) {
        this.realizationStart = realizationStart;
    }

    public Date getRealizationEnd() {
        return realizationEnd;
    }

    public void setRealizationEnd(Date realizationEnd) {
        this.realizationEnd = realizationEnd;
    }

    public List<PdaVisitCargoRealization> getItems() {
        return items;
    }

    public void setItems(List<PdaVisitCargoRealization> items) {
        this.items = items;
    }

    public List<PdaVisitLocationRealization> getLocations() {
        return locations;
    }

    public void setLocations(List<PdaVisitLocationRealization> locations) {
        this.locations = locations;
    }
}
