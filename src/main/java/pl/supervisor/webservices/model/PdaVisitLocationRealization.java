package pl.supervisor.webservices.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by norbertl on 11.05.16.
 */
public class PdaVisitLocationRealization implements Serializable{

    private String pointSymbol;
    private Date beginPresenceTime;
    private Date endPresenceTime;

    // trzy poniższe dla punktów bez symbolu !
    // ale proszę wypełniaj zawsze tym co przychodzi w zleceniu (zakładam że mogą dać i symbol i inne współrzędne)
    // wtedy współrzędne są ważniejszej niż to co w symbolu
    private Double latitude;
    private Double longitude;
    private Integer radius; // tego nie ma w VisitWsTo, więc  zostaw null jeśli nie ma symbolu punktu, jak jest to wpisz promień z punktu

    // dodałem tutaj na wypadek gdyby nie było poprawnego czasu - żeby ocenić co w jakiej kolejności było odwiedzone
    private Integer indexInRoute;

    public String getPointSymbol() {
        return pointSymbol;
    }

    public void setPointSymbol(String pointSymbol) {
        this.pointSymbol = pointSymbol;
    }

    public Date getBeginPresenceTime() {
        return beginPresenceTime;
    }

    public void setBeginPresenceTime(Date beginPresenceTime) {
        this.beginPresenceTime = beginPresenceTime;
    }

    public Date getEndPresenceTime() {
        return endPresenceTime;
    }

    public void setEndPresenceTime(Date endPresenceTime) {
        this.endPresenceTime = endPresenceTime;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Integer getRadius() {
        return radius;
    }

    public void setRadius(Integer radius) {
        this.radius = radius;
    }

    public Integer getIndexInRoute() {
        return indexInRoute;
    }

    public void setIndexInRoute(Integer indexInRoute) {
        this.indexInRoute = indexInRoute;
    }
}
