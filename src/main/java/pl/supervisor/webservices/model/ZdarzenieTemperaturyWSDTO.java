package pl.supervisor.webservices.model;

import java.util.Date;

public class ZdarzenieTemperaturyWSDTO {

	private Long id;
	
	private String numerRejestracyjny;
	private Date dataCzasPoczatku;
	
	private Integer numerTermometru;
	private Double temperatura;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNumerRejestracyjny() {
		return numerRejestracyjny;
	}
	public void setNumerRejestracyjny(String numerRejestracyjny) {
		this.numerRejestracyjny = numerRejestracyjny;
	}
	public Date getDataCzasPoczatku() {
		return dataCzasPoczatku;
	}
	public void setDataCzasPoczatku(Date dataCzasPoczatku) {
		this.dataCzasPoczatku = dataCzasPoczatku;
	}
	public Integer getNumerTermometru() {
		return numerTermometru;
	}
	public void setNumerTermometru(Integer numerTermometru) {
		this.numerTermometru = numerTermometru;
	}
	public Double getTemperatura() {
		return temperatura;
	}
	public void setTemperatura(Double temperatura) {
		this.temperatura = temperatura;
	}
	
}
