package pl.supervisor.webservices.model;

import javax.xml.bind.annotation.*;
import java.io.Serializable;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="Pasazer")
@XmlType(name="Pasazer")
public class Pasazer implements Serializable{

    @XmlElement(nillable = false, required = true)
    private String idPasazera;
    @XmlElement(nillable = false, required = true)
    private String nazwaPasazera1;
    @XmlElement(nillable = true, required = false)
    private String nazwaPasazera2;
    @XmlElement(nillable = true, required = false)
    private String numerTelefonuDoPowiadomienia;
    @XmlElement(nillable = true, required = false)
    private Boolean powiadomicPrzedPrrzyjazdem;
    @XmlElement(nillable = true, required = false)
    private Integer iloscKmPrzedPowiadomieniem;
    @XmlElement(nillable = true, required = false)
    private String dodatkoweInformacje1;
    @XmlElement(nillable = true, required = false)
    private String dodatkoweInformacje2;
    @XmlElement(nillable = false, required = true)
    private RUCH_PASAZERA rodzajRuchu;

    public enum RUCH_PASAZERA{
        WEJSCIE, WYJSCIE
    }

    public String getIdPasazera() {
        return idPasazera;
    }

    public void setIdPasazera(String idPasazera) {
        this.idPasazera = idPasazera;
    }

    public String getNazwaPasazera1() {
        return nazwaPasazera1;
    }

    public void setNazwaPasazera1(String nazwaPasazera1) {
        this.nazwaPasazera1 = nazwaPasazera1;
    }

    public String getNazwaPasazera2() {
        return nazwaPasazera2;
    }

    public void setNazwaPasazera2(String nazwaPasazera2) {
        this.nazwaPasazera2 = nazwaPasazera2;
    }

    public String getNumerTelefonuDoPowiadomienia() {
        return numerTelefonuDoPowiadomienia;
    }

    public void setNumerTelefonuDoPowiadomienia(String numerTelefonuDoPowiadomienia) {
        this.numerTelefonuDoPowiadomienia = numerTelefonuDoPowiadomienia;
    }

    public Boolean getPowiadomicPrzedPrrzyjazdem() {
        return powiadomicPrzedPrrzyjazdem;
    }

    public void setPowiadomicPrzedPrrzyjazdem(Boolean powiadomicPrzedPrrzyjazdem) {
        this.powiadomicPrzedPrrzyjazdem = powiadomicPrzedPrrzyjazdem;
    }

    public Integer getIloscKmPrzedPowiadomieniem() {
        return iloscKmPrzedPowiadomieniem;
    }

    public void setIloscKmPrzedPowiadomieniem(Integer iloscKmPrzedPowiadomieniem) {
        this.iloscKmPrzedPowiadomieniem = iloscKmPrzedPowiadomieniem;
    }

    public String getDodatkoweInformacje1() {
        return dodatkoweInformacje1;
    }

    public void setDodatkoweInformacje1(String dodatkoweInformacje1) {
        this.dodatkoweInformacje1 = dodatkoweInformacje1;
    }

    public String getDodatkoweInformacje2() {
        return dodatkoweInformacje2;
    }

    public void setDodatkoweInformacje2(String dodatkoweInformacje2) {
        this.dodatkoweInformacje2 = dodatkoweInformacje2;
    }

    public RUCH_PASAZERA getRodzajRuchu() {
        return rodzajRuchu;
    }

    public void setRodzajRuchu(RUCH_PASAZERA rodzajRuchu) {
        this.rodzajRuchu = rodzajRuchu;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Pasazer{");
        sb.append("idPasazera=").append(idPasazera);
        sb.append(", nazwaPasazera1='").append(nazwaPasazera1).append('\'');
        sb.append(", nazwaPasazera2='").append(nazwaPasazera2).append('\'');
        sb.append(", numerTelefonuDoPowiadomienia='").append(numerTelefonuDoPowiadomienia).append('\'');
        sb.append(", powiadomicPrzedPrrzyjazdem=").append(powiadomicPrzedPrrzyjazdem);
        sb.append(", iloscKmPrzedPowiadomieniem=").append(iloscKmPrzedPowiadomieniem);
        sb.append(", dodatkoweInformacje1='").append(dodatkoweInformacje1).append('\'');
        sb.append(", dodatkoweInformacje2='").append(dodatkoweInformacje2).append('\'');
        sb.append(", rodzajRuchu=").append(rodzajRuchu);
        sb.append('}');
        return sb.toString();
    }
}
