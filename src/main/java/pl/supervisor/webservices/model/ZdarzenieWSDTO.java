package pl.supervisor.webservices.model;

import java.util.Date;

public class ZdarzenieWSDTO implements java.io.Serializable{
    
	private String numerRejestracyjny;
	private String zdarzenie;
	private Date dataCzasPoczatku;
	private Date dataCzasKonca;
	private String kierowca;
	private Double dystans;
	
	public ZdarzenieWSDTO() {
		super();
	}
		
	public ZdarzenieWSDTO(String numerRejestracyjny, String zdarzenie,
			Date dataCzasPoczatku, Date dataCzasKonca, String kierowca,
			Double dystans) {
		super();
		this.numerRejestracyjny = numerRejestracyjny;
		this.zdarzenie = zdarzenie;
		this.dataCzasPoczatku = dataCzasPoczatku;
		this.dataCzasKonca = dataCzasKonca;
		this.kierowca = kierowca;
		this.dystans = dystans;
	}

	public String getNumerRejestracyjny() {
		return numerRejestracyjny;
	}

	public void setNumerRejestracyjny(String numerRejestracyjny) {
		this.numerRejestracyjny = numerRejestracyjny;
	}

	public String getZdarzenie() {
		return zdarzenie;
	}

	public void setZdarzenie(String zdarzenie) {
		this.zdarzenie = zdarzenie;
	}

	public Date getDataCzasPoczatku() {
		return dataCzasPoczatku;
	}

	public void setDataCzasPoczatku(Date dataCzasPoczatku) {
		this.dataCzasPoczatku = dataCzasPoczatku;
	}

	public Date getDataCzasKonca() {
		return dataCzasKonca;
	}

	public void setDataCzasKonca(Date dataCzasKonca) {
		this.dataCzasKonca = dataCzasKonca;
	}

	public String getKierowca() {
		return kierowca;
	}

	public void setKierowca(String kierowca) {
		this.kierowca = kierowca;
	}

	public Double getDystans() {
		return dystans;
	}

	public void setDystans(Double dystans) {
		this.dystans = dystans;
	}

	@Override
	public String toString() {
		return "ZdarzenieWSDTO ["
				+ (numerRejestracyjny != null ? "numerRejestracyjny="
						+ numerRejestracyjny + ", " : "")
				+ (zdarzenie != null ? "zdarzenie=" + zdarzenie + ", " : "")
				+ (dataCzasPoczatku != null ? "dataCzasPoczatku="
						+ dataCzasPoczatku + ", " : "")
				+ (dataCzasKonca != null ? "dataCzasKonca=" + dataCzasKonca
						+ ", " : "")
				+ (kierowca != null ? "kierowca=" + kierowca + ", " : "")
				+ (dystans != null ? "dystans=" + dystans : "") + "]";
	}	
	
	
}
