package pl.supervisor.webservices.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by norbertl on 17.06.16.
 */
public class MapPointTO implements Serializable {

    private Double latitude;
    private Double longitude;
    private String description;
    private Date dateTime;
    private String batteryLevel;
    private String identificationMode;
    private Integer locationPrecision;
    private String[] classes;
    private String country;
    private String voivodeship;
    private String city;
    private String street;
    private String zip;
    private Double cumulativeDistance;
    private Long cumulativeTime;

    public MapPointTO() {
    }

    public MapPointTO(Double latitude, Double longitude, String description, Date dateTime, String[] classes) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.description = description;
        this.dateTime = dateTime;
        this.classes = classes;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public String[] getClasses() {
        return classes;
    }

    public void setClasses(String[] classes) {
        this.classes = classes;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getVoivodeship() {
        return voivodeship;
    }

    public void setVoivodeship(String voivodeship) {
        this.voivodeship = voivodeship;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public Double getCumulativeDistance() {
        return cumulativeDistance;
    }

    public void setCumulativeDistance(Double cumulativeDistance) {
        this.cumulativeDistance = cumulativeDistance;
    }

    public Long getCumulativeTime() {
        return cumulativeTime;
    }

    public void setCumulativeTime(Long cumulativeTime) {
        this.cumulativeTime = cumulativeTime;
    }

    public boolean hasValidCoordinates() {

        return getLongitude() != null && getLongitude() != null;
    }

    public String getBatteryLevel() {
        return batteryLevel;
    }

    public void setBatteryLevel(String batteryLevel) {
        this.batteryLevel = batteryLevel;
    }

    public String getIdentificationMode() {
        return identificationMode;
    }

    public void setIdentificationMode(String identificationMode) {
        this.identificationMode = identificationMode;
    }

    public Integer getLocationPrecision() {
        return locationPrecision;
    }

    public void setLocationPrecision(Integer locationPrecision) {
        this.locationPrecision = locationPrecision;
    }
}