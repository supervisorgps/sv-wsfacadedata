package pl.supervisor.webservices.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by norbertl on 19.10.16.
 */
public class PdaVisitCargoRealization2 implements Serializable{

    private String cargoId;
    private String cargoAdditionalInfo1;
    private String cargoAdditionalInfo2;
    private String cargoAdditionalInfo3; // poziom naładowania baterii

    private Double longitude;
    private Double latitude;

    private Integer locationPrecision;

    private String cargoOperation; // IN/OUT

    private Date operationDate;

    private String identificationMode; // beacon - manual - ?

    public PdaVisitCargoRealization2(PdaVisitCargoRealization item) {

        this.cargoId = item.getCargoId();
        this.cargoAdditionalInfo1 = item.getCargoAdditionalInfo1();
        this.cargoAdditionalInfo2 = item.getCargoAdditionalInfo2();

        this.latitude = item.getLatitude();
        this.longitude = item.getLongitude();

        this.locationPrecision = item.getLocationPrecision();
        this.cargoOperation = item.getCargoOperation();

        this.operationDate = item.getOperationDate();
    }

    public PdaVisitCargoRealization2(){}

    public String getCargoId() {
        return cargoId;
    }

    public void setCargoId(String cargoId) {
        this.cargoId = cargoId;
    }

    public String getCargoAdditionalInfo1() {
        return cargoAdditionalInfo1;
    }

    public void setCargoAdditionalInfo1(String cargoAdditionalInfo1) {
        this.cargoAdditionalInfo1 = cargoAdditionalInfo1;
    }

    public String getCargoAdditionalInfo2() {
        return cargoAdditionalInfo2;
    }

    public void setCargoAdditionalInfo2(String cargoAdditionalInfo2) {
        this.cargoAdditionalInfo2 = cargoAdditionalInfo2;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Integer getLocationPrecision() {
        return locationPrecision;
    }

    public void setLocationPrecision(Integer locationPrecision) {
        this.locationPrecision = locationPrecision;
    }

    public String getCargoOperation() {
        return cargoOperation;
    }

    public void setCargoOperation(String cargoOperation) {
        this.cargoOperation = cargoOperation;
    }

    public Date getOperationDate() {
        return operationDate;
    }

    public void setOperationDate(Date operationDate) {
        this.operationDate = operationDate;
    }

    public String getIdentificationMode() {
        return identificationMode;
    }

    public void setIdentificationMode(String identificationMode) {
        this.identificationMode = identificationMode;
    }

    public String getCargoAdditionalInfo3() {
        return cargoAdditionalInfo3;
    }

    public void setCargoAdditionalInfo3(String cargoAdditionalInfo3) {
        this.cargoAdditionalInfo3 = cargoAdditionalInfo3;
    }
}
