package pl.supervisor.webservices.model;

import java.io.Serializable;
import java.util.*;

public class PdaPosition implements Serializable {

    private String vehicleRegistrationNumber;
    private String driverName;
    private String driverLogin;
    private String orderSymbol;
    private Long orderNumber;

    private Date positionDateTime;
    private Double vehicleSpeed;
    private String positionAddress;
    private Boolean inOrder = false;

    private Double distanceDuringCurrentOrder;
    private Date currentOrderBegin;

    private Double lat;
    private Double lon;

    private Double realizationStartLat;
    private Double realizationStartLon;

    private Double realizationEndLat;
    private Double realizationEndLon;

    private Set<String> cargoIds = new HashSet<String>();

    private List<MapPointTO> specialPoints = new ArrayList<MapPointTO>();

    private Map<String ,Object[]> additionalDataMap = new HashMap<String ,Object[] >();

    public void addCargoId(String cargoId){
        cargoIds.add( cargoId);
    }
    public String getVehicleRegistrationNumber() {
        return vehicleRegistrationNumber;
    }

    public void setVehicleRegistrationNumber(String vehicleRegistrationNumber) {
        this.vehicleRegistrationNumber = vehicleRegistrationNumber;
    }

    public void addSpecialPoint(MapPointTO specialPoint){

        specialPoints.add(specialPoint);
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getOrderSymbol() {
        return orderSymbol;
    }

    public void setOrderSymbol(String orderSymbol) {
        this.orderSymbol = orderSymbol;
    }

    public Date getPositionDateTime() {
        return positionDateTime;
    }

    public void setPositionDateTime(Date positionDateTime) {
        this.positionDateTime = positionDateTime;
    }

    public Double getVehicleSpeed() {
        return vehicleSpeed;
    }

    public void setVehicleSpeed(Double vehicleSpeed) {
        this.vehicleSpeed = vehicleSpeed;
    }

    public String getPositionAddress() {
        return positionAddress;
    }

    public void setPositionAddress(String positionAddress) {
        this.positionAddress = positionAddress;
    }

    public Boolean getInOrder() {
        return inOrder;
    }

    public void setInOrder(Boolean inOrder) {
        this.inOrder = inOrder;
    }

    public Double getDistanceDuringCurrentOrder() {
        return distanceDuringCurrentOrder;
    }

    public void setDistanceDuringCurrentOrder(Double distanceDuringCurrentOrder) {
        this.distanceDuringCurrentOrder = distanceDuringCurrentOrder;
    }

    public Date getCurrentOrderBegin() {
        return currentOrderBegin;
    }

    public void setCurrentOrderBegin(Date currentOrderBegin) {
        this.currentOrderBegin = currentOrderBegin;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public Long getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(Long orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getDriverLogin() {
        return driverLogin;
    }

    public void setDriverLogin(String driverLogin) {
        this.driverLogin = driverLogin;
    }

    public Set<String> getCargoIds() {
        return cargoIds;
    }

    public void setCargoIds(Set<String> cargoIds) {
        this.cargoIds = cargoIds;
    }

    public List<MapPointTO> getSpecialPoints() {
        return specialPoints;
    }

    public void setSpecialPoints(List<MapPointTO> specialPoints) {
        this.specialPoints = specialPoints;
    }

    public Double getRealizationStartLat() {
        return realizationStartLat;
    }

    public void setRealizationStartLat(Double realizationStartLat) {
        this.realizationStartLat = realizationStartLat;
    }

    public Double getRealizationStartLon() {
        return realizationStartLon;
    }

    public void setRealizationStartLon(Double realizationStartLon) {
        this.realizationStartLon = realizationStartLon;
    }

    public Double getRealizationEndLat() {
        return realizationEndLat;
    }

    public void setRealizationEndLat(Double realizationEndLat) {
        this.realizationEndLat = realizationEndLat;
    }

    public Double getRealizationEndLon() {
        return realizationEndLon;
    }

    public void setRealizationEndLon(Double realizationEndLon) {
        this.realizationEndLon = realizationEndLon;
    }

    public Map<String, Object[]> getAdditionalDataMap() {
        return additionalDataMap;
    }

    public void setAdditionalDataMap(Map<String, Object[]> additionalDataMap) {
        this.additionalDataMap = additionalDataMap;
    }
}
