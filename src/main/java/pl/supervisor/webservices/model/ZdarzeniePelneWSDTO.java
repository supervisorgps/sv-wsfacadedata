package pl.supervisor.webservices.model;

import org.codehaus.jackson.annotate.JsonIgnore;
import pl.supervisor.frontend.gwt.model.client.ICalculateTurnovers;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
//@XmlJavaTypeAdapter(value = UtcTimestampAdapter.class, type = Date.class)
public class ZdarzeniePelneWSDTO implements Serializable, ICalculateTurnovers, Comparable<ZdarzeniePelneWSDTO> {

	private Long id;
	private String numerRejestracyjny;
	private String zdarzenie;

    private Date dataCzasPoczatku;
    private Date dataCzasKonca;
	private Integer lostSignalTime = 0; // tylko dla jazda

	private Double fuelAtStart;
	private Double fuelAtStop;
	private Double startLongitude;
	private Double startLatitude;
	private String startStreet;
	private String startCountry;
	private String startCounty;
	private String startCity;
	private String startZip;

	private Double endLongitude;
	private Double endLatitude;
	private String endStreet;
	private String endCountry;
	private String endCounty;
	private String endCity;
	private String endZip;

	private String driver;

	private Integer czasTrwaniaSek = 0;
	private Integer dystans = 0;

	private Integer vMax = 0;
	private Integer vGr = 0;

	private Double vSr = 0.0d;

	private Integer czasProg1;
	private Integer czasProg2;
	private Integer czasProg3;

	private String poiStart;
	private String poiEnd;

	private int turnoversScope0;
	private int turnoversScope1;
	private int turnoversScope2;
	private int turnoversScope3;

	private Integer iloscGrM2;
	private Integer szerokoscSypania;
	private Integer szerokoscSypaniaNaStrony;
	private Integer bajtInfo;

	private Long pdaImei;
	private boolean planowanieZlecenie = false;
	private String numerZlecenia;
	private Long adnId;

	private Integer czasJazdyLimitPredkosci1;
	private Integer czasJazdyLimitPredkosci2;
	private Integer czasJazdyLimitPredkosci3;
	private Integer czasJazdyLimitPredkosci4;

	private Double zuzycieCanTotal;

	public ZdarzeniePelneWSDTO() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNumerRejestracyjny() {
		return numerRejestracyjny;
	}

	public void setNumerRejestracyjny(String numerRejestracyjny) {
		this.numerRejestracyjny = numerRejestracyjny;
	}

	public String getZdarzenie() {
		return zdarzenie;
	}

	public void setZdarzenie(String zdarzenie) {
		this.zdarzenie = zdarzenie;
	}

    //	@XmlSchemaType(name = "dateTime", type = XMLGregorianCalendar.class)
    public Date getDataCzasPoczatku() {
		return dataCzasPoczatku;
	}

	public Double getFuelAtStart() {
		return fuelAtStart;
	}

	public void setFuelAtStart(Double fuelAtStart) {
		this.fuelAtStart = fuelAtStart;
	}

	public Double getFuelAtStop() {
		return fuelAtStop;
	}

	public void setFuelAtStop(Double fuelAtStop) {
		this.fuelAtStop = fuelAtStop;
	}

	public String getDriver() {
		return driver;
	}

	public void setDriver(String driver) {
		this.driver = driver;
	}

	public void setDataCzasPoczatku(Date dataCzasPoczatku) {
		this.dataCzasPoczatku = dataCzasPoczatku;
	}

    //	@XmlSchemaType(name = "dateTime", type = XMLGregorianCalendar.class)
    public Date getDataCzasKonca() {
		return dataCzasKonca;
	}

	public void setDataCzasKonca(Date dataCzasKonca) {
		this.dataCzasKonca = dataCzasKonca;
	}

	public Integer getLostSignalTime() {
		return lostSignalTime;
	}

	public void setLostSignalTime(Integer lostSignalTime) {
		this.lostSignalTime = lostSignalTime;
	}

	public Double getStartLongitude() {
		return startLongitude;
	}

	public void setStartLongitude(Double startLongitude) {
		this.startLongitude = startLongitude;
	}

	public Double getStartLatitude() {
		return startLatitude;
	}

	public void setStartLatitude(Double startLatitude) {
		this.startLatitude = startLatitude;
	}

	public String getStartStreet() {
		return startStreet;
	}

	public void setStartStreet(String startStreet) {
		this.startStreet = startStreet;
	}

	public String getStartCountry() {
		return startCountry;
	}

	public void setStartCountry(String startCountry) {
		this.startCountry = startCountry;
	}

	public String getStartCounty() {
		return startCounty;
	}

	public void setStartCounty(String startCounty) {
		this.startCounty = startCounty;
	}

	public String getStartCity() {
		return startCity;
	}

	public void setStartCity(String startCity) {
		this.startCity = startCity;
	}

	public String getStartZip() {
		return startZip;
	}

	public void setStartZip(String startZip) {
		this.startZip = startZip;
	}

	public Double getEndLongitude() {
		return endLongitude;
	}

	public void setEndLongitude(Double endLongitude) {
		this.endLongitude = endLongitude;
	}

	public Double getEndLatitude() {
		return endLatitude;
	}

	public void setEndLatitude(Double endLatitude) {
		this.endLatitude = endLatitude;
	}

	public String getEndStreet() {
		return endStreet;
	}

	public void setEndStreet(String endStreet) {
		this.endStreet = endStreet;
	}

	public String getEndCountry() {
		return endCountry;
	}

	public void setEndCountry(String endCountry) {
		this.endCountry = endCountry;
	}

	public String getEndCounty() {
		return endCounty;
	}

	public void setEndCounty(String endCounty) {
		this.endCounty = endCounty;
	}

	public String getEndCity() {
		return endCity;
	}

	public void setEndCity(String endCity) {
		this.endCity = endCity;
	}

	public String getEndZip() {
		return endZip;
	}

	public void setEndZip(String endZip) {
		this.endZip = endZip;
	}

	public Integer getCzasTrwaniaSek() {
		return czasTrwaniaSek;
	}

	public void setCzasTrwaniaSek(Integer czasTrwaniaSek) {
		this.czasTrwaniaSek = czasTrwaniaSek;
	}

	public Integer getDystans() {
		return dystans;
	}

	public void setDystans(Integer dystans) {
		this.dystans = dystans;
	}

    public Integer getVMax() {
        return vMax;
	}

    public void setVMax(Integer vMax) {
        this.vMax = vMax;
	}

    public Integer getVGr() {
        return vGr;
	}

    public void setVGr(Integer vGr) {
        this.vGr = vGr;
	}

    public Double getVSr() {
        return vSr;
	}

    public void setVSr(Double vSr) {
        this.vSr = vSr;
	}

	public Integer getCzasProg1() {
		return czasProg1;
	}

	public void setCzasProg1(Integer czasProg1) {
		this.czasProg1 = czasProg1;
	}

	public Integer getCzasProg2() {
		return czasProg2;
	}

	public void setCzasProg2(Integer czasProg2) {
		this.czasProg2 = czasProg2;
	}

	public Integer getCzasProg3() {
		return czasProg3;
	}

	public void setCzasProg3(Integer czasProg3) {
		this.czasProg3 = czasProg3;
	}

	public String getPoiStart() {
		return poiStart;
	}

	public void setPoiStart(String poiStart) {
		this.poiStart = poiStart;
	}

	public String getPoiEnd() {
		return poiEnd;
	}

	public void setPoiEnd(String poiEnd) {
		this.poiEnd = poiEnd;
	}

	@Override
	public Date getStartTime() {
		return dataCzasPoczatku;
	}

	@Override
	public Date getEndTime() {

		if ( null == dataCzasKonca){

			return dataCzasPoczatku;
		}

		return dataCzasKonca;
	}

	@Override
    @JsonIgnore
    public int getTurnoversInScope1() {
		return czasProg1.intValue();
	}

	@Override
    @JsonIgnore
    public int getTurnoversInScope2() {
		return czasProg2.intValue();
	}

	@Override
    @JsonIgnore
    public int getTurnoversInScope3() {
		return czasProg3.intValue();
	}

	@Override
    @JsonIgnore
    public void setTurnoversInScope0AsPercent(int percentage) {
		this.turnoversScope0 = percentage;
	}

	@Override
    @JsonIgnore
    public void setTurnoversInScope1AsPercent(int percentage) {
        this.turnoversScope1 = percentage;
    }

	@Override
    @JsonIgnore
    public void setTurnoversInScope2AsPercent(int percentage) {
        this.turnoversScope2 = percentage;
    }

	@Override
    @JsonIgnore
    public void setTurnoversInScope3AsPercent(int percentage) {
        this.turnoversScope3 = percentage;
    }

    public int getTurnoversScope0() {
        return turnoversScope0;
    }

    public void setTurnoversScope0(int turnoversScope0) {
        this.turnoversScope0 = turnoversScope0;
    }

    public int getTurnoversScope1() {
        return turnoversScope1;
    }

    public void setTurnoversScope1(int turnoversScope1) {
        this.turnoversScope1 = turnoversScope1;
    }

    public int getTurnoversScope2() {
        return turnoversScope2;
    }

    public void setTurnoversScope2(int turnoversScope2) {
        this.turnoversScope2 = turnoversScope2;
    }

    public int getTurnoversScope3() {
        return turnoversScope3;
    }

    public void setTurnoversScope3(int turnoversScope3) {
        this.turnoversScope3 = turnoversScope3;
    }

	public Integer getIloscGrM2() {
		return iloscGrM2;
	}

	public void setIloscGrM2(Integer iloscGrM2) {
		this.iloscGrM2 = iloscGrM2;
	}

	public Integer getSzerokoscSypania() {
		return szerokoscSypania;
	}

	public void setSzerokoscSypania(Integer szerokoscSypania) {
		this.szerokoscSypania = szerokoscSypania;
	}

	public Integer getSzerokoscSypaniaNaStrony() {
		return szerokoscSypaniaNaStrony;
	}

	public void setSzerokoscSypaniaNaStrony(Integer szerokoscSypaniaNaStrony) {
		this.szerokoscSypaniaNaStrony = szerokoscSypaniaNaStrony;
	}

	public Integer getBajtInfo() {
		return bajtInfo;
	}

	public void setBajtInfo(Integer bajtInfo) {
		this.bajtInfo = bajtInfo;
	}

	public Long getPdaImei() {
		return pdaImei;
	}

	public void setPdaImei(Long pdaImei) {
		this.pdaImei = pdaImei;
	}

	public boolean getPlanowanieZlecenie() {
		return planowanieZlecenie;
	}

	public void setPlanowanieZlecenie(boolean planowanieZlecenie) {
		this.planowanieZlecenie = planowanieZlecenie;
	}

	public String getNumerZlecenia() {
		return numerZlecenia;
	}

	public void setNumerZlecenia(String numerZlecenia) {
		this.numerZlecenia = numerZlecenia;
	}

	@Override
	public int compareTo(ZdarzeniePelneWSDTO o) {

        if ( null == getDataCzasPoczatku()){

            return 1;

		} else if (  null == o.getDataCzasPoczatku()){

            return -1;
		}

        return getDataCzasPoczatku().compareTo( o.getDataCzasPoczatku());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ZdarzeniePelneWSDTO other = (ZdarzeniePelneWSDTO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Long getAdnId() {
		return adnId;
	}

	public void setAdnId(Long adnId) {
		this.adnId = adnId;
	}

	public Integer getCzasJazdyLimitPredkosci1() {
		return czasJazdyLimitPredkosci1;
	}

	public void setCzasJazdyLimitPredkosci1(Integer czasJazdyLimitPredkosci1) {
		this.czasJazdyLimitPredkosci1 = czasJazdyLimitPredkosci1;
	}

	public Integer getCzasJazdyLimitPredkosci2() {
		return czasJazdyLimitPredkosci2;
	}

	public void setCzasJazdyLimitPredkosci2(Integer czasJazdyLimitPredkosci2) {
		this.czasJazdyLimitPredkosci2 = czasJazdyLimitPredkosci2;
	}

	public Integer getCzasJazdyLimitPredkosci3() {
		return czasJazdyLimitPredkosci3;
	}

	public void setCzasJazdyLimitPredkosci3(Integer czasJazdyLimitPredkosci3) {
		this.czasJazdyLimitPredkosci3 = czasJazdyLimitPredkosci3;
	}

	public Integer getCzasJazdyLimitPredkosci4() {
		return czasJazdyLimitPredkosci4;
	}

	public void setCzasJazdyLimitPredkosci4(Integer czasJazdyLimitPredkosci4) {
		this.czasJazdyLimitPredkosci4 = czasJazdyLimitPredkosci4;
	}

	public Double getZuzycieCanTotal() {
		return zuzycieCanTotal;
	}

	public void setZuzycieCanTotal(Double zuzycieCanTotal) {
		this.zuzycieCanTotal = zuzycieCanTotal;
	}

}
