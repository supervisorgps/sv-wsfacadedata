package pl.supervisor.webservices.model;

import java.io.Serializable;
import java.util.Date;

public class AutoDaneDto implements Serializable {

private Long adnId;
	
	private Integer adnKlnId;
	
	private boolean adnRoaming;
	
	private String adnNrRejestracyjny;
	
	private boolean adnAktywny;
	
	private String adnDataDezaktywacji;
	
	private Integer adnAudytKtoAktywny;

	public Long getAdnId() {
		return adnId;
	}

	public void setAdnId(Long adnId) {
		this.adnId = adnId;
	}

	public Integer getAdnKlnId() {
		return adnKlnId;
	}

	public void setAdnKlnId(Integer adnKlnId) {
		this.adnKlnId = adnKlnId;
	}

	public boolean getAdnRoaming() {
		return adnRoaming;
	}

	public void setAdnRoaming(boolean adnRoaming) {
		this.adnRoaming = adnRoaming;
	}

	public String getAdnNrRejestracyjny() {
		return adnNrRejestracyjny;
	}

	public void setAdnNrRejestracyjny(String adnNrRejestracyjny) {
		this.adnNrRejestracyjny = adnNrRejestracyjny;
	}

	public boolean getAdnAktywny() {
		return adnAktywny;
	}

	public void setAdnAktywny(boolean adnAktywny) {
		this.adnAktywny = adnAktywny;
	}

	public String getAdnDataDezaktywacji() {
		return adnDataDezaktywacji;
	}

	public void setAdnDataDezaktywacji(String adnDataDezaktywacji) {
		this.adnDataDezaktywacji = adnDataDezaktywacji;
	}

	public Integer getAdnAudytKtoAktywny() {
		return adnAudytKtoAktywny;
	}

	public void setAdnAudytKtoAktywny(Integer adnAudytKtoAktywny) {
		this.adnAudytKtoAktywny = adnAudytKtoAktywny;
	}
}
