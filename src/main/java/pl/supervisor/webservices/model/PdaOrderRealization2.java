package pl.supervisor.webservices.model;

import java.io.Serializable;
import java.util.*;

/**
 * Created by norbertl on 19.10.16.
 */
public class PdaOrderRealization2 implements Serializable{

    private static final long serialVersionUID = 1001L;

    private Integer orderId;
    private String orderSymbol;

    private String driverLogin;
    private Date realizationStart;
    private Double realizationStartLat;
    private Double realizationStartLon;

    private Date realizationEnd;
    private Double realizationEndLat;
    private Double realizationEndLon;

    private Map<String ,Object[]> additionalDataMap = new HashMap<String ,Object[] >();

    public PdaOrderRealization2( PdaOrderRealization old) {

        orderId = old.getOrderId();
        orderSymbol = old.getOrderSymbol();

        driverLogin = old.getDriverLogin();
        realizationStart = old.getRealizationStart();

        realizationEnd = old.getRealizationEnd();

        if ( old.getItems() != null){

            items = new ArrayList<PdaVisitCargoRealization2>();

            for( PdaVisitCargoRealization item : old.getItems()){

                items.add( new PdaVisitCargoRealization2( item));
            }
        }

        if ( old.getLocations() != null){

            locations = new ArrayList<PdaVisitLocationRealization2>();

            for( PdaVisitLocationRealization loc : old.getLocations()){

                locations.add( new PdaVisitLocationRealization2( loc));
            }
        }
    }

    public PdaOrderRealization2() {
    }

    private List<PdaVisitCargoRealization2> items = new ArrayList<PdaVisitCargoRealization2>(0);

    private List<PdaVisitLocationRealization2> locations = new ArrayList<PdaVisitLocationRealization2>();

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getOrderSymbol() {
        return orderSymbol;
    }

    public void setOrderSymbol(String orderSymbol) {
        this.orderSymbol = orderSymbol;
    }

    public String getDriverLogin() {
        return driverLogin;
    }

    public void setDriverLogin(String driverLogin) {
        this.driverLogin = driverLogin;
    }

    public Date getRealizationStart() {
        return realizationStart;
    }

    public void setRealizationStart(Date realizationStart) {
        this.realizationStart = realizationStart;
    }

    public Date getRealizationEnd() {
        return realizationEnd;
    }

    public void setRealizationEnd(Date realizationEnd) {
        this.realizationEnd = realizationEnd;
    }

    public List<PdaVisitCargoRealization2> getItems() {
        return items;
    }

    public void setItems(List<PdaVisitCargoRealization2> items) {
        this.items = items;
    }

    public List<PdaVisitLocationRealization2> getLocations() {
        return locations;
    }

    public void setLocations(List<PdaVisitLocationRealization2> locations) {
        this.locations = locations;
    }

    public Double getRealizationStartLat() {
        return realizationStartLat;
    }

    public void setRealizationStartLat(Double realizationStartLat) {
        this.realizationStartLat = realizationStartLat;
    }

    public Double getRealizationStartLon() {
        return realizationStartLon;
    }

    public void setRealizationStartLon(Double realizationStartLon) {
        this.realizationStartLon = realizationStartLon;
    }

    public Double getRealizationEndLat() {
        return realizationEndLat;
    }

    public void setRealizationEndLat(Double realizationEndLat) {
        this.realizationEndLat = realizationEndLat;
    }

    public Double getRealizationEndLon() {
        return realizationEndLon;
    }

    public void setRealizationEndLon(Double realizationEndLon) {
        this.realizationEndLon = realizationEndLon;
    }

    public Map<String, Object[]> getAdditionalDataMap() {
        return additionalDataMap;
    }

    public void setAdditionalDataMap(Map<String, Object[]> additionalDataMap) {
        this.additionalDataMap = additionalDataMap;
    }
}
