package pl.supervisor.webservices.model;

import java.io.Serializable;

/**
 * Created by norbertl on 22.08.16.
 */
public class PdaOrderRequest implements Serializable{

    private Long orderNumber;
    private String driverLogin;

    // 1 - planned, 2 - done
    private int orderType;

    public Long getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(Long orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getDriverLogin() {
        return driverLogin;
    }

    public void setDriverLogin(String driverLogin) {
        this.driverLogin = driverLogin;
    }

    public int getOrderType() {
        return orderType;
    }

    public void setOrderType(int orderType) {
        this.orderType = orderType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PdaOrderRequest that = (PdaOrderRequest) o;

        if (orderType != that.orderType) return false;
        if (!orderNumber.equals(that.orderNumber)) return false;
        return driverLogin.equals(that.driverLogin);

    }

    @Override
    public int hashCode() {
        int result = orderNumber.hashCode();
        result = 31 * result + driverLogin.hashCode();
        result = 31 * result + orderType;
        return result;
    }
}
